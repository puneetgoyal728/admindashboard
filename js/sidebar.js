$(window).scroll(function() {
	var window_pos = $(window).scrollTop();
	if (window_pos < 208) {
		var currpos = (208 - window_pos) > 75 ? (208 - window_pos) : 75;
		$('#left_area').css('top', currpos + 'px');
	} else {
		$('#left_area').css('top', '75px');
	}
});