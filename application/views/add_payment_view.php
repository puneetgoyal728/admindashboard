<aside class="right-side">
   <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active" >Make Payments</a></li>
        </ol>
   </section>
   <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
          <!-- <li class="active"><a href="#tab_1-1" data-toggle="tab"></a></li> -->
          <li class="pull-left header"><i class="fa fa-credit-card"></i>Make Payment</li>
        </ul>
    </div>
    <br/>

    <form method="post" class=" " action="<?php echo base_url() ?>payC/add_payment" id = "add_payment" />     
      
      <div class="basic-grey">
        <label>
          <span> Email :</span>
          <input type="text" id="email" placeholder= "Search.." name="email" value="" /> <br/>
        </label>
        <div id="fillData"></div>
      </div>
  
      <div class = "payment_form basic-grey" id = "cash">
        <label>
          <span>Amount </span>
          <input type="text" name="cash_amount"/>
        </label>  
          <br/>
      </div>   

      <div class = "payment_form basic-grey" id = "net_bank" />           
        <label>
          <span>Amount </span>
          <input type="text" name="net_amount"/>
        </label>
         <br/>
        <label>
          <span>Transaction ID </span>
          <input type="text" name="net_bnk_tid"/>
        </label>
         <br/>    
        <label>
          <span>A/c Name </span>
          <input type="text" name="net_acc_name"/>
        </label>
         <br/>
        <label>
          <span>A/c No.  </span>
          <input type="text" name="net_acc_no"/>
        </label>
         <br/>  
        <label>
          <span>Bank Name  </span>
          <input type="text" name="net_bnk_name"/>
        </label>
         <br/>
        <label>
          <span>Bank Branch  </span>
          <input type="text" name="net_bnk_branch"/>
        </label>
         <br/>
        <label>
          <span>IFSC Code  </span>
          <input type="text" name="net_ifsc_code"/>
        </label>
         <br/>
        <!-- <input type="submit" value="ADD"/>  -->
      </div>      
    
      <div class = "payment_form basic-grey" id = "cheque" />                 
        <label>
          <span>Amount </span>
          <input type="text" name="dd_amount"/>
        </label>
         <br/>
        <label>
          <span>Cheque/DD No.  </span>
          <input type="text" name="dd_cheq_no"/>
        </label>
         <br/>
        <label>
          <span>Date Issued  </span>
          <input type="text" name="dd_cheq_date"/>
        </label>
         <br/>
        <label>
          <span>A/c Name  </span>
          <input type="text" name="dd_acc_name"/>
        </label>
         <br/>
        <label>
          <span>Bank Name  </span>
          <input type="text" name="dd_bnk_name"/>
        </label>
         <br/>
        <label>
          <span>Bank Branch  </span>
          <input type="text" name="dd_bnk_branch"/>
        </label>
         <br/>
        <!-- <input type="submit" value="ADD"/> -->
      </div> 

      <div class = "payment_form basic-grey" id = "add_button" />
        <input class="btn btn-success btn-flat" type="submit" value="ADD"/>
      </div>

    </form>

    <div class = "payment_form basic-grey" id = "postpaid-login" />
        <h5 id="pp-msg">Please re enter your password to confirm this action.</h5>
        <input type="hidden" name = "username" value="<?php echo $username; ?>" />
        <label>
          <span>Password </span>
          <input type="password" id="password" name="password"/>
        </label>
         <br/>
         <button class="btn btn-success btn-flat" onclick="confirm_pp()">Confirm</button>
    </div>

    
    <div style="display:none" id="emailList"></div>


<!-- <script src="<?php //echo base_url() ?>js/jquery-1.11.1.min.js"></script> -->
<!-- <script src="<?php //echo base_url() ?>jquery-ui/jquery-ui.js"></script>  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>jquery-ui/jquery-ui.css"></link>
<!-- http://api.jqueryui.com/autocomplete/ -->
<script>

$(".payment_form").hide();
$(document).ready(function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() ?>usersC/list_cli_email",
    }).done(function (data){
      $('#emailList').text(data);
      $( "#email" ).autocomplete({
         // Comment lines 2248-2253, 2295-2601.. in library.
         // $( "<div>" ).text( label ).appendTo( this.liveRegion );
         source: JSON.parse($.trim(data))
      });
      $( "#email" ).on( "autocompleteselect", function(event, ui){
        checkEmail(ui.item.value);
      });
    });
});


// http://stackoverflow.com/questions/11919065/sort-an-array-by-the-levenshtein-distance-with-best-performance-in-javascript
// http://www.merriampark.com/ld.htm, http://www.mgilleland.com/ld/ldjavascript.htm, Damerau–Levenshtein distance (Wikipedia)



function checkEmail(email) {
    // document.getElementsByClassName("payment_form").display="none";
    // if(selVal)
    // var email = document.getElementById("email").value;
    // alert(" Email Address, " + email);
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    var select = $('<div class = "user-detail"><label><span>Type :</span><select name="pay_mode" id = "pay_mode"/></label></div>');
    var option = { 'm0': '--Mode of Pay--', 'm1': 'Cash', 'm2': 'Net Banking', 'm3': 'Cheque/DD', 'm4': 'Post Paid', 'm5': 'Free Credits' }
    

    if(re.test(email))
    {
    	//ajax
      $(".user-detail").remove();
    	$.ajax({
    		 type: "POST",
    		 url: "<?php echo base_url() ?>usersC/fetch_details",
    		 data: {'email': email }
    	}).done(function (data){
            var ret_data=JSON.parse(data);
        		$("#fillData").append(" <div class='user-detail'><input type='hidden' name = 'user_id' value='"+ ret_data['id']+"' /><label><span> ID : </span><input type='text' value='"+ ret_data['id']+"' disabled></input></label><label><span> Name : </span><input type='text' value='"+ ret_data['username']+"' disabled></input></label></div>");
            // $("#fillData").append(" <h5 class='user-detail '> Mode OF Payment : </h5> </div>");
            $("#fillData").append(select);
            for(var val in option)
            {
              $('<option />', {value: val, text: option[val]}).appendTo($('#pay_mode'));
            }
            $("#pay_mode").change( function(e){
               var pay_mode = $("#pay_mode option:selected").val();
               $(".payment_form").hide();
               switch(pay_mode){
                
                case 'm1':
                    $("#add_payment").show();
                    $("#cash").show();
                    $("#add_button").show();
                    break;
                
                case 'm2':
                    $('#add_payment').show();
                    $("#net_bank").show();
                    $("#add_button").show();
                    break;
                
                case 'm3':
                    $('#add_payment').show();
                    $("#cheque").show();
                    $("#add_button").show();
                    break; 
                
                case 'm4':
                    $('#add_payment').show();
                    $("#pp-msg").html("Please re enter your password to confirm this action.");
                    $("#password").val("");
                    $("#postpaid-login").show();
                    break; 
                case 'm5':
                    $('#add_payment').show();
                    $("#cash").show();
                    $("#add_button").show();
                    break;              

               }
            });
    	});
    }else{
    	alert("Invalid Email Address, Try Again !!!");
    }
}

function confirm_pp(argument) {
  // body...

  var password = document.getElementById("password").value;
// <form method="post" action="<?php echo base_url() ?>login/confirm_pp" id = "add_pp" />
  $.ajax({
         type: "POST",
         url: "<?php echo base_url() ?>login/confirm_pp",
         data: { 'username' : "<?php echo $username; ?>", 'password': password }
      }).done(function (data){
        // alert("returned  " + data);
        if (data == 1) 
        {
          $("#postpaid-login").hide();
          $("#cash").show();
          $("#add_button").show();
        }
        else 
        {
          document.getElementById("pp-msg").innerHTML = data;
          $("#postpaid-login").show();
        }

        
      });
}

</script>
</aside>