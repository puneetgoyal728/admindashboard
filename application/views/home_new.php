
  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
   <!-- <h2>Welcome <?php // echo $username; ?> !</h2> -->
   <h4 class="success-msg"><?php  if(isset($msg)){echo $msg;} ?></h4>
  <!--  <div>
	  <a class="button" href="<?php //echo base_url() ?>jobsC/list_all_jobs">List all jobs</a>
   	  <?php //echo $last_job; ?> 
   </div> -->
  <!--  <div >
	  <a class="button" href="<?php //echo base_url() ?>usersC/list_all_users">List all Users</a>
   	  <?php //echo $last_user; ?>
   </div> -->
   <!-- <div >
	  <a class="button" href="<?php //echo base_url() ?>payC/list_all_pay">List all Payments</a>
   	  <?php //echo $last_pay; ?>
   </div> -->
   <!-- <br/>
   <a class="btn btn-primary btn-flat" href="<?php // echo base_url() ?>home/logout">Logout</a>
   <a class="btn btn-primary btn-flat" href="<?php // echo base_url() ?>home/make_payment">Make Payment</a>
   <a class="btn btn-primary btn-flat" href="<?php echo base_url() ?>home/load_graph">Graph</a>
 -->

    <?php
      $session_data = $this->session->userdata('logged_in'); 
      if ($session_data['accesslevel'] <3) {
          echo '<div class="row"> <div class="col-md-12 not_3"> <div class="box box-solid "> <div class="box-header"> <i class="fa fa-bar-chart-o"></i> <h3 class="box-title">Payment Flow</h3> <div class="box-tools pull-right"> <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>';
          echo '</div> </div> <div class="box-body border-radius-none"> <div class="chart" id="revenue-chart" style="position: relative;"></div> </div> </div> </div> </div>';
      }
    ?>

   <div class="row">
   <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Last Added Job</h3>
            <!-- <div class="box-tools pull-right">
                <div class="btn btn-primary btn-flat"><?php //echo $last_job_box['Client']; ?> </div>
            </div> -->
        </div>
        <div class="box-body">
            <code>Job Name  </code><?php echo $last_job_box['Job_Name']; ?> <br/>
            <code>Client:   </code><?php echo $last_job_box['Client']; ?> <br/>
            <code># Pages  </code><?php echo $last_job_box['Pages']; ?> <br/>
            <code>When  </code><?php echo $last_job_box['When']; ?> <br/>
            
        </div><!-- /.box-body -->
        <div class="box-footer">
            <a class="btn btn-primary btn-flat" href="<?php echo base_url() ?>jobsC/list_all_jobs">List all jobs </a>
        </div><!-- /.box-footer-->
      </div>
   </div>

   <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Last User Activity</h3>
        </div>
        <div class="box-body">
            
               <code>Client  </code><?php echo $last_user_box['Client']; ?> <br/>
               <code>Activity  </code><?php echo $last_user_box['Activity']; ?> <br/>
               <code>When  </code><?php echo $last_user_box['When']; ?> <br/>
          
        </div><!-- /.box-body -->
        <div class="box-footer">
            <a class="btn btn-primary btn-flat" href="<?php echo base_url() ?>usersC/list_all_users">List all Users</a>
        </div><!-- /.box-footer-->
      </div>
   </div>

   <?php
      if ($session_data['accesslevel'] <3) {
          echo '<div class="col-md-3 not_3"> <div class="box box-primary"> <div class="box-header"> <h3 class="box-title">Last Payment</h3> </div> <div class="box-body">';
          echo '<code>Amount   </code>'.$last_pay_box["Amount"].'<br/>';
          echo '<code>Client:   </code>'.$last_pay_box["Client"].'<br/>';
          echo '<code>Mode:   </code>'.$last_pay_box["Mode"].'<br/>';
          echo '<code>When:   </code>'.$last_pay_box["When"].'<br/></div>';
          echo '<div class="box-footer"> <code><a class="btn btn-primary btn-flat" href="'.base_url() .'payC/list_all_pay">List all Payments</a></code>';
          echo '</div></div></div>';
      }
    ?>
   <!-- <div class="col-md-3 not_3">
      <div class="box box-primary">
         <div class="box-header">
            <h3 class="box-title">Last Payment</h3>
            
         </div>
         <div class="box-body">
            <code>Amount   </code><?php echo $last_pay_box['Amount']; ?> <br/>
            <code>Client:   </code><?php echo $last_pay_box['Client']; ?> <br/>
            <code>Mode:   </code><?php echo $last_pay_box['Mode']; ?> <br/>
            <code>When:   </code><?php echo $last_pay_box['When']; ?> <br/>

         </div> --><!-- /.box-body -->
           <!-- <div class="box-footer">
               <code><a class="btn btn-primary btn-flat" href="<?php echo base_url() ?>payC/list_all_pay">List all Payments</a></code>
           </div> --><!-- /.box-footer-->
       <!-- </div> --><!-- /.box -->
   <!-- </div> --> 

 </div>

   <script>

        $(document).ready(function(){
            $.ajax({
              type: "POST",
              url: "<?php echo base_url() ?>payC/load_graph",
            }).done(function (a_data){
              if (a_data) {
                new_data =  JSON.parse(a_data);
              // alert(a_data); 
                var area_in = new Morris.Area({
                  element: 'revenue-chart',
                  resize: true,
                  data: new_data,
                  xkey: 'date',
                  ykeys: ['out', 'in'],
                  labels: ['Out Flow', 'In Flow'],
                  lineColors: ['#ee0000', '#00ee00'],
                  hideHover: 'auto'
                });
              };
            });
        });    

    </script>
  </aside>
