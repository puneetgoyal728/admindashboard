
<!-- <div class="wrapper row-offcanvas row-offcanvas-left" style="min-height: 389px;"> -->
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas" style="min-height: 855px;">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo base_url() ?>img/avatar5.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>Hello, 
                        <?php 
                        $bar_active;
                            $session_data = $this->session->userdata('logged_in');  
                            echo $session_data['username']; 
                        ?>
                    </p>

                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

             <ul class="sidebar-menu">
                <li <?php if ($bar_active == "HOME") {
                    echo 'style="background-color: #00d3ff"';
                }?> >
                    <a href="<?php echo base_url() ?>">
                        <i class="fa fa-th"></i> <span>Home</span> 
                    </a>
                </li>
                <li <?php if ($bar_active == "USERS") {
                    echo 'style="background-color: #00d3ff"';
                }?> >
                    <a href="<?php echo base_url() ?>usersC/list_all_users">
                        <i class="fa fa-users" ></i> <span>Users</span>
                        <!-- glyphicon glyphicon-user -->
                    </a>
                </li>
                <li <?php if ($bar_active == "JOBS") {
                    echo 'style="background-color: #00d3ff"';
                }?> >
                    <a href="<?php echo base_url() ?>jobsC/list_all_jobs">
                        <i class="fa fa-tasks"></i> <span>Jobs</span> 
                    </a>
                </li>
                <?php
                    if ($session_data['accesslevel'] <3) {
                        echo '<li ';
                        if ($bar_active == "PAYS") {
                            echo 'style="background-color: #00d3ff"';
                        }
                        echo '> <a href= '.base_url().'payC/list_all_pay> <i class="fa fa-money"></i> <span>Payments</span> </a> </li>';
                    }
                ?>
                <?php
                    if ($session_data['accesslevel'] <2) {
                        echo '<li ';
                        if ($bar_active == "ADDPAY") {
                            echo 'style="background-color: #00d3ff"';
                        }
                        echo '> <a href='.base_url().'home/make_payment> <i class="fa fa-credit-card"></i> <span>Make Payment</span> </a> </li>';

                        echo '<li ';
                        if ($bar_active == "ADDUSER") {
                            echo 'style="background-color: #00d3ff"';
                        }
                        echo '> <a href='.base_url().'home/add_user> <i class="fa fa-user"></i> <span>Add User</span> </a> </li>';

                        echo '<li ';
                        if ($bar_active == "ADDADMIN") {
                            echo 'style="background-color: #00d3ff"';
                        }
                        echo '> <a href='.base_url().'home/add_admin> <i class="fa  fa-asterisk"></i> <span>Add Admin</span> </a> </li>';
                        
                    }
                ?>
                
                <!-- <li <?php if ($bar_active == "ADDPAY" and $session_data['accesslevel'] <2) {
                    echo 'style="background-color: #00d3ff"';
                }?> class="not_3 not_2">
                    <a href="<?php echo base_url() ?>home/make_payment">
                        <i class="fa fa-credit-card"></i> <span>Make Payment</span> 
                    </a>
                </li> -->
                <!-- <li <?php if ($bar_active == "ADDUSER" and $session_data['accesslevel'] <2) {
                    echo 'style="background-color: #00d3ff"';
                }?> class="not_3 not_2">
                    <a href="<?php echo base_url() ?>home/add_user">
                        <i class="fa fa-user"></i> <span>Add User</span> 
                    </a>
                </li> -->
                <!-- <li <?php if ($bar_active == "ADDADMIN" and $session_data['accesslevel'] <2) {
                    echo 'style="background-color: #00d3ff"';
                }?> class="not_3 not_2">
                    <a href="<?php echo base_url() ?>home/add_admin">
                        <i class="fa  fa-asterisk"></i> <span>Add Admin</span> 
                    </a>
                </li> -->
                <li>
                    <a href="<?php echo base_url() ?>home/logout">
                        <i class="fa fa-power-off"></i> <span>Log Out</span> 
                    </a>
                </li>
             </ul>
        </section>
    </aside>


 