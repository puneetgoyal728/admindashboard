<aside class="right-side">
 	
    <!-- <button class="btn btn-primary btn-flat" id="edit_button" onClick = "show_edit_user()">Edit User</button> -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url() ?>usersC/list_all_users">Users</a></li>
            <li class="active">Details</li>
        </ol>
    </section>
    

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
            <li class="active"><a href="#tab_1-1" data-toggle="tab">Details</a></li>
            <li><a href="#tab_2-2" data-toggle="tab" onClick = "return cancel_edit(event);">Jobs</a></li>
            <li><a href="#tab_2-3" data-toggle="tab" onClick = "return cancel_edit(event);">Templates</a></li>
            <li>
                <?php
                    if ($userlevel == 1) {
                        $act_btn = "<button class='btn btn-primary btn-flat' id='deactivate' onClick = 'deactivate_user()'>Deactivate</button>";
                        // echo $act_btn;
                    } 
                    elseif ($userlevel == 0) {
                        $act_btn = "<button class='btn btn-primary btn-flat' id='deactivate' onClick = 'deactivate_user()'>Activate</button>";
                        // echo $act_btn;
                    }
                    $edit_btn = '<button class="btn btn-primary btn-flat" id="edit_button" onClick = "show_edit_user()">Edit User</button>';
                ?>
            </li>
            <!-- <li> <button class="btn btn-primary btn-flat" id="edit_button" onClick = "show_edit_user()">Edit User</button></li> -->
            <li class="pull-left header"><i class="glyphicon glyphicon-user"></i><strong>User&nbsp;:</strong> &nbsp; <?php echo $row_data[0]; ?> </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1-1">
                <div class="box">
                    <!-- <div class="box-header">
                        <h3 class="box-title">User Detail : <?php echo $user_id; ?> </h3>
                    </div> -->
                    <div class="box-body no-padding">
                        <?php
                            $tmpl = array (
                              'table_open' => '<table class="table table-bordered">',
                              'table_close' => '</table>'
                            );
                            $this->table->set_template($tmpl); 
                            $this->table->set_heading('Client', 'Email', 'Mobile', 'Company', 'Action', 'Edit');
                            array_push($row_data, $act_btn, $edit_btn);
                            $this->table->add_row($row_data);
                            // $this->table->set_caption('User Details :'); 
                            echo $this->table->generate();
                            // echo $user_id."<br/>";
                        ?>
                    </div><!-- /.box-body -->
                </div>
                
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2-2">
                <div class="box">
                    <div class="box-body no-padding">
                        <?php echo $job_data; ?>
                    </div><!-- /.box-body -->
                </div>
                
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2-3">
                <div class="box">
                    <div class="box-body no-padding">
                        <?php echo $temp_data; ?>
                    </div><!-- /.box-body -->
                </div>
                
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div>

     <div class="form-box edit_user_form" style="margin: 10px 0 0 0" id="add_user">
        <!-- <div class="header">Edit User Details</div> -->
            <?php echo form_open('usersC/edit_user'); ?>
                <div class = "body">           
                    <label for="name">Name :</label>
                     <input class="form-control" type="text" name="name" value="<?php echo $row_data[0]; ?>"/>
                     <label class="control-label"> <?php echo form_error('name'); ?> </label>
                     <br/>
                    <label for="email_id">Email ID :</label>
                     <input class="form-control" type="text" name="email_id" value="<?php echo $row_data[1]; ?>" disabled=""/>
                     <br/> 
                    <label for="mobile_no">Mobile :</label>
                     <input class="form-control" type="text" name="mobile_no" value="<?php echo $row_data[2]; ?>"/>
                     <label class="control-label"> <?php echo form_error('mobile_no'); ?> </label>
                     <br/>  
                    <label for="company">Company Name :</label>
                     <input class="form-control" type="text" name="company" value="<?php echo $row_data[3]; ?>"/>
                     <label class="control-label"> <?php echo form_error('company'); ?> </label>
                     <br/>
                     <input class="form-control" type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                     <input type="hidden" name="row_data" value="<?php echo form_prep(json_encode($row_data)); ?>" />
                </div> 
                <div class="footer">
                    <input class="btn btn-success btn-block" type="submit" value="Save"/> 
                    <button class="btn btn-danger btn-block" id="cancel_button" onClick = "return cancel_edit(event);">Cancel</button>
                </div>
            </form>
    </div>  


	<script src="<?php echo base_url() ?>js/jquery.min.js"></script>
	<?php if (!isset($reload)) {
    echo '<script>$(".edit_user_form").hide(); </script>';
    } else {
        // echo '<script>$("#edit_button").hide(); </script>';
    }
    ?>
    <script>  
    
        var userlevel=<?php echo $userlevel; ?>;
        
        function show_edit_user() {
            $(".edit_user_form").show();
            // $("#edit_button").hide();
        }

        function cancel_edit(event) {
            // event.stopPropagation();
            // event.preventDefault();
            $(".edit_user_form").hide();
            // $("#edit_button").show();
            // window.location = ""
            return false;
        }

        function deactivate_user() {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>usersC/change_userlevel",
                data: {'user_id': <?php echo $user_id; ?>, 'level': userlevel }
            }).done(function (data){

                if (data == 1) {
                    document.getElementById("deactivate").innerHTML = "Deactivate";
                    userlevel=1;
                } else if (data == 0) {
                    document.getElementById("deactivate").innerHTML = "Activate";   
                    userlevel=0;
                }
             });
        }

	</script>



</aside>