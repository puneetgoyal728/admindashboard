 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
   <style>
      
        
   </style> 

        <!-- <title> <?php echo $title; ?></title> -->
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url() ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url() ?>css/morris/morris.css" rel="stylesheet" type="text/css" />

 </head>
 <body>

   <div id="wrapper">
     <div id="container">
        <div class="box box-solid bg-aqua-gradient">
           <div class="box-header">
                <i class="fa fa-th"></i>
                <h3 class="box-title">Sales Graph</h3>
                <div class="box-tools pull-right">
                    <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <!-- <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                </div>
            </div>
            <div class="box-body border-radius-none">
                <div class="chart" id="revenue-chart" style="position: relative; width: 500px; height: 300px;"></div>                                    
            </div><!-- /.box-body -->
        </div><!-- /.box -->                            
     </div><!-- end container -->
   </div><!-- end wrapper -->


    <script src="<?php echo base_url() ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>jquery-ui/jquery-ui.js"></script>

    <!-- Bootstrap -->
    <script src="<?php echo base_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>js/AdminLTE/app.js" type="text/javascript"></script>    
    <!-- Morris.js charts -->
    <script src="<?php echo base_url() ?>js/raphael-min.js"></script>
    <script src="<?php echo base_url() ?>js/plugins/morris/morris.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url() ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo base_url() ?>js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
    
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo base_url() ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url() ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

    <!-- AdminLTE for demo purposes -->
    <!-- <script src="<?php echo base_url() ?>js/pay_graph.js"></script> -->
    
    <script>

        $(document).ready(function(){
            $.ajax({
              type: "POST",
              url: "<?php echo base_url() ?>payC/load_graph",
            }).done(function (a_data){
              // alert(JSON.parse(a_data));  
              var area = new Morris.Area({
                element: 'revenue-chart',
                resize: true,
                data: JSON.parse(a_data),
                // data: [
                //     {y: '2011 Q1', item1: 2666, item2: 2666},
                //     {y: '2011 Q2', item1: 2778, item2: 2294},
                //     {y: '2011 Q3', item1: 4912, item2: 1969},
                //     {y: '2011 Q4', item1: 3767, item2: 3597},
                //     {y: '2012 Q1', item1: 6810, item2: 1914},
                //     {y: '2012 Q2', item1: 5670, item2: 4293},
                //     {y: '2012 Q3', item1: 4820, item2: 3795},
                //     {y: '2012 Q4', item1: 15073, item2: 5967},
                //     {y: '2013 Q1', item1: 10687, item2: 4460},
                //     {y: '2013 Q2', item1: 8432, item2: 5713}
                // ],
                xkey: 'dat',
                ykeys: ['amt'],
                labels: ['Sales'],
                lineColors: ['#00ee00'],
                hideHover: 'auto'
            });
            });
        });    


            

    </script>

 </body>
</html>