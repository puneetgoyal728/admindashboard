 <aside class="right-side">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active" >Jobs</li>
        </ol>
    </section>
 	<div class="col-md">
    <!-- Custom Tabs (Pulled to the right) -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
            <li class="active"><a href="#tab_1-1" data-toggle="tab">Pending</a></li>
            <li><a href="#tab_2-2" data-toggle="tab">Completed</a></li>
           <!--  <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Dropdown <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
            </li> -->
            <li class="pull-left header"><i class="fa fa-edit"></i> Job Details</li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1-1">
                <div class="box">
					<div class="box-body no-padding">
						<?php echo $pending_jobs; ?>
					</div><!-- /.box-body -->
				</div>
                
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2-2">
                <div class="box">
					<div class="box-body no-padding">
						<?php echo $completed_jobs; ?>
					</div><!-- /.box-body -->
				</div>
                
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->
	</div>
</aside>
