<aside class="right-side">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Users</li>
        </ol>
    </section>
	<div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
        	<li class="active"><a href="#tab_1-1" data-toggle="tab">Activities</a></li>
            <li><a href="#tab_2-2" data-toggle="tab">Details</a></li>
            <li class="pull-left header"><i class="fa fa-users"></i> All Users</li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1-1">
                <div class="box">
					<div class="box-body no-padding">
						<?php echo $all_activites; ?>
					</div><!-- /.box-body -->
				</div>
                
            </div><!-- /.tab-pane -->
             
            <div class="tab-pane" id="tab_2-2">
                <div class="box">
					<div class="box-body no-padding">
						<?php echo $all_details; ?>
					</div><!-- /.box-body -->
				</div>
                
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div>
</aside>




    



    