<?php
session_start();
if(!isset($_SESSION["uid"])){
    header("location:logx.php");
}
include_once("config.php");
include_once ('db_conn.php');
include_once 'common_func.php';
$us = $_SESSION["uid"];
$formid=$_GET["g"];
if(!isset($_GET["j"]))
{
 foreach( $conn->dbh->query("SELECT data_img_name ,min(page_no) page FROM tbl_image_master where form_id = $formid and active=1" ) as $row)
     {
         $curpage =$row['page'];
         $l=$row['data_img_name'];
     }
}
else 
{
  $curpage =$_GET["j"];
 foreach( $conn->dbh->query("SELECT data_img_name  FROM tbl_image_master where form_id = $formid and page_no=$curpage" ) as $row)
     {
         $l=$row['data_img_name'];
     }
}
list($width, $height, $type, $attr) = getimagesize($l);
/* Buttons */
$pages[]=0;
$index=0;
$pageButtons = "<div class='center' style='padding:20px 0px 20px 0px; width:600px; text-align:right;'>";
foreach( $conn->dbh->query("SELECT *  FROM tbl_image_master where  form_id=$formid and active = 1") as $arr)
{
    $pages[$index]=$arr['page_no'];
    ++$index;
}
if($index==1)
{
    $pageButtons .= "<a class = 'button' style='opacity: 0.3; '>prev</a>";
    $pageButtons .= "<a class = 'button' style='opacity: 0.3; '>next</a>";

}
else {

    for($df=0;$df<$index;++$df)
    {
        if($pages[$df]==$curpage)
        {
            if($df==0)
            {
                $next=$pages[$df+1];
                $prev=$pages[$df];
                $pageButtons .= "<a class = 'button' style='opacity: 0.3; ' >prev</a>";
                $pageButtons .= "<a class = 'button' href=templatepreview.php?g=".$formid."&j=".$next.">next</a>";
            }
            else if($df==$index-1)
            {
                $next=$pages[$df];
                $prev=$pages[$df-1];
                $pageButtons .= "<a class = 'button' href=templatepreview.php?g=".$formid."&j=".$prev.">prev</a>";
                $pageButtons .= "<a class = 'button' style='opacity: 0.3;'>next</a>";
            }
            else
            {
                $next=$pages[$df+1];
                $prev=$pages[$df-1];
                $pageButtons .= "<a class = 'button' href=templatepreview.php?g=".$formid."&j=".$prev.">prev</a>";
                $pageButtons .= "<a class = 'button' href=templatepreview.php?g=".$formid."&j=".$next.">next</a>";
            }
        }

    }
}

$pageButtons .= '<a class = "button" id ="finishbutton" href="template.php";>finish</a>';
$pageButtons .= "</div>";
/* Buttons */

$ark=array();
foreach(  $conn->dbh->query("SELECT * FROM tbl_template_master where form_id =$formid and page_no=$curpage") as $ro)
{
 $data=array("id"=>$ro['field_id'],"coordinates"=>$ro['field_cord'],"name"=>$ro['field_name'],"type"=>$ro['field_type'],"sub_type"=>$ro['sub_field_type'],"details"=>$ro['field_details'],"instructions"=>$ro['instructions']); 
 array_push($ark, $data)  ;
}
$deg = json_encode( $ark );
$currentHead = "TEMPLATE";
include('new_header.php');
include('subheader.php');
echo $pageButtons;

?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="<?php echo auto_version('/css/default.css');?>" />
<title>Movable and Re-sizable Raphael JS Shape</title>
</head>
<body>

    <style>
#left_area {
    background-color: white;
    border: 1px #cccccc solid;
    width: 300px;
    position: fixed;
    right: 0;
    padding: 20px 10px;
    border-right: 0px;
    top: 230;
}

#leftheader {
    position: fixed;
    top: 50px;
    left: 1000px;
    color: red;
}

textarea.pos_fixed {
    margin-top: 1px;
    color: red;
}

#dropdown {
    margin-top: 10px;
    color: red;
}

#sub_multiple {
    margin-top: 10px;
    color: red;
}

#sub_title {
    margin-top: 10px;
    color: red;
}

#choices {
    margin-top: 10px;
}

.textinput {
    margin-top: 10px;
}
</style>
<div id="paper"></div>
<div id="left_area">
        <h3 id= "msg_pane">This template is locked</h3>
       
    </div>
<script src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>
<script src="<?php echo auto_version('/js/raphael.min.js');?>"></script>
<script src="<?php echo auto_version('/js/sidebar.js');?>"></script>
<script>
$('#left_area').show();
var cr=0;
function imageratio(hr, wr) {
    var zr;
    var hr = hr/ 1433;
    var wr = wr/900;

    if (wr > hr) {
        if (hr >= 1 && wr >= 1) {
            imgwid = imgwid / wr;
            imghei = imghei / wr;
            zr = wr;
        } else if (hr <= 1 && wr >= 1) {
            imgwid = imgwid / wr;
            imghei = imghei / wr;
            zr = wr;

        } else {
            imgwid = imgwid / wr;
            imghei = imghei / wr;
            zr = wr;
        }
    } else if (wr < hr) {
        if (hr >= 1 && wr >= 1) {
            imgwid = imgwid / hr;
            imghei = imghei / hr;
            zr = hr;
        } else if (hr >= 1 && wr <= 1) {
            imgwid = imgwid / hr;
            imghei = imghei / hr;
            zr = hr;
        } else {
            imgwid = imgwid / hr;
            imghei = imghei / hr;
            zr = hr;
        }

    }

    else if (hr == wr) {

        if (hr > 1) {
            imgwid = imgwid / hr;
            imghei = imghei / hr;
            zr = hr;
        }

        else if (hr < 1) {

            imgwid = imgwid / hr;
            imghei = imghei / hr;
            zr = hr;

        }

    }

    return zr;
}

var jay= <?php echo json_encode($ark ); ?>;
var  imgwid = "<?php echo $width;?>"; 
var  imghei = "<?php echo $height;?>"; 
var zr=imageratio(imghei,imgwid);
var crflag=0;
// Create drawing area
var paper = Raphael("paper", 915, 1500);
var php_var = "<?php echo $l; ?>";
var img1 = paper.image(php_var,0,0,imgwid,imghei);
$(img1.node).on('dragstart',function(event){event.preventDefault();});
var fields=[];

function field (arr) {
	var resx = arr["coordinates"].split(",");
    this.recs=paper.rect((resx[0].trim())/zr,(resx[1].trim())/zr,((resx[2])/zr)-(resx[0]/zr),((resx[3]/zr))-(resx[1]/zr));
    this.recs.attr("fill", "BLUE");
    this.recs.attr("fill-opacity", "0.10");
    this.id=arr["id"];
    this.name=arr["name"];
    this.type=arr["type"];
    this.sub_type=arr["sub_type"];
    this.details=arr["details"];
    this.instructions=arr["instructions"];

    this.recs.click(function (event, a, b) {
      recsclick(arr);
     });
}

for(i in jay)
{
 var f = new field(jay[i]);
 fields.push(f);
}
var data;
function recsclick(rec)
{
 $(".data").remove();
 for(i in rec)
    $('#left_area').append('<h3 class="data">'+rec[i]+'</h3>');

}


</script>
    <p id="demo"></p>
</body>
</html>
