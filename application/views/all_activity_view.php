<aside class="right-side">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url() ?>usersC/list_all_users">Users</a></li>
            <li class="active">Activities</li>
        </ol>
    </section>
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">All Activities of <?php echo $user_name; ?></h3>
		</div>
		<div class="box-body no-padding">
			<?php echo $all_act; ?>
		</div><!-- /.box-body -->
	</div>
 </aside>>