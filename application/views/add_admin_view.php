 <aside class="right-side">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active" >Add Admin</a></li>
        </ol>
   </section>
   <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
          <!-- <li class="active"><a href="#tab_1-1" data-toggle="tab"></a></li> -->
          <li class="pull-left header"><i class="fa fa-asterisk"></i>Create New Admin</li>
        </ul>
    </div>
    <div class="form-box" style="margin: 10px 0 0 0" id="add_user">
        <!-- <h3><i class="fa fa-user"></i>  Add New User</h3> -->
            <?php echo form_open('home/new_admin'); ?>
                <div class = "body ">           
                    
                     <input type="text" class="form-control" placeholder= "Name" name="name" value="<?php echo set_value('name'); ?>"/>
                     <label class="control-label"> <?php echo form_error('name'); ?> </label>
                     <br/>
                     <input type="text" class="form-control" placeholder= "Email ID" name="email_id" value="<?php echo set_value('email_id'); ?>"/>
                     <label class="control-label"> <?php echo form_error('email_id'); ?> </label>
                     <br/>    
                     <input type="text" class="form-control" placeholder= "Password" name="password" value="<?php echo set_value('password'); ?>"/>
                     <label class="control-label"> <?php echo form_error('password'); ?> </label>
                     <br/>
                     <input type="text" class="form-control" placeholder= "Access Level" name="accesslevel" value="<?php echo set_value('accesslevel'); ?>"/>
                     <label class="control-label"> <?php echo form_error('accesslevel'); ?> </label>
                     <br/>
                </div>    
                <div class="footer">  
                    <input class="btn btn-default btn-block" type="submit" value="Register"/> 
                    <a class="btn btn-danger btn-block" href="<?php echo base_url() ?>home/new_home">Cancel</a>
                </div>    
            </form>
        </div>    
</aside>