<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
   <style>
      h4.success-msg{
            color: blueviolet;
        }
       a.deactive{
            color:red;
       }
       a.active{
            color:green;
       } 



   </style> 

        <title> <?php echo $title; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url() ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url() ?>css/morris/morris.css" rel="stylesheet" type="text/css" />

 </head>

    <script src="<?php echo auto_version('/js/jquery.min.js') ?>"></script>
    <!--<script src="<?php echo base_url() ?>js/jquery.min.js"></script>-->
    <script src="<?php echo base_url() ?>jquery-ui/jquery-ui.min.js"></script>

    <!-- Bootstrap -->
    <script src="<?php echo base_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>js/dashboard.js" type="text/javascript"></script>
    <!-- Morris.js charts -->
    <script src="<?php echo base_url() ?>js/raphael-min.js"></script>
    <script src="<?php echo base_url() ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url() ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo base_url() ?>js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
    
    <!-- Bootstrap WYSIHTML5 -->
   <!-- <script src="<?php echo base_url() ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script> -->
    <!-- iCheck -->
    <script src="<?php echo base_url() ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

 <body>
   <div id="wrapper">
     <div id="container">
       <div class="wrapper row-offcanvas row-offcanvas-left" style="min-height: 389px;">
        <div id="top">
            <?php $this->load->view('header_view');?>
        </div>

        <div id="main">
            <?php $this->load->view($main_body);?>
        </div>
       </div>  
        <div id="footer"> 
            <?php // $this->load->view('bottom'); ?>
        </div>
     </div><!-- end container -->
   </div><!-- end wrapper -->

 </body>
</html>