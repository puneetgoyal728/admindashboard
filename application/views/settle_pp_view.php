<aside class="right-side">
  <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url() ?>payC/list_all_pay">Payments</a></li>
            <li class="active">Add Pending Pay</li>
        </ol>
  </section>
  <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
          <li class="pull-left header"><i class="fa fa-credit-card"></i>Add Pending Payment</li>
        </ul>
  </div>
    <form method="post" action="<?php echo base_url() ?>payC/settle_pp" id = "settle_pp" />     
    
      <div class="basic-grey">
          <label><span>Client ID : </span><input type="text" value="<?php echo $id; ?>" disabled=""></label>
          <label><span>Client : </span><input type="text" value="<?php echo $name; ?>" disabled=""></label>
          <label><span>Amount : </span><input type="text" value="<?php echo $amount; ?>" disabled=""></label>
        <div id="fillData" ></div>
      </div>
      <!-- <div id="fillData" ></div> -->

      <div class = "payment_form basic-grey", id = "cash">
        <label>
          <span>Amount </span>
          <input type="text" name="cash_amount"/>
        </label>
          <br/>
      </div>   

      <div class = "payment_form basic-grey", id = "net_bank" />           
        <label>
          <span>Amount </span>
          <input type="text" name="net_amount"/>
        </label>
         <br/>
        <label>
          <span>Transaction ID </span>
          <input type="text" name="net_bnk_tid"/>
        </label>
         <br/>    
        <label>
          <span>A/c Name </span>
          <input type="text" name="net_acc_name"/>
        </label>
         <br/>
        <label>
          <span>A/c No. </span>
          <input type="text" name="net_acc_no"/>
        </label>
         <br/>  
        <label>
          <span>Bank Name </span>
          <input type="text" name="net_bnk_name"/>
        </label>
         <br/>
        <label>
          <span>Bank Branch </span>
          <input type="text" name="net_bnk_branch"/>
        </label>
         <br/>
        <label>
          <span>IFSC Code </span>
          <input type="text" name="net_ifsc_code"/>
        </label>
         <br/>
        <!-- <input type="submit" value="ADD"/>  -->
      </div>      
    
      <div class = "payment_form basic-grey", id = "cheque" />                 
        <label>
          <span>Amount </span>
          <input type="text" name="dd_amount"/>
        </label>
         <br/>
        <label>
          <span>Cheque/DD No. </span>
          <input type="text" name="dd_cheq_no"/>
        </label>
         <br/>
        <label>
          <span>Date Issued </span>
          <input type="text" name="dd_cheq_date"/>
        </label>
         <br/>
        <label>
          <span>A/c Name </span>
          <input type="text" name="dd_acc_name"/>
        </label>
         <br/>
        <label>
          <span>Bank Name </span>
          <input type="text" name="dd_bnk_name"/>
        </label>
         <br/>
        <label>
          <span>Bank Branch </span>
          <input type="text" name="dd_bnk_branch"/>
        </label>
         <br/>
        
      </div> 

      <div class = "payment_form basic-grey", id = "add_button" />
        <input type='hidden' name = "form_type" value="settle_pp" />
        <input type='hidden' name = "amount_pp" value="<?php echo $amount; ?>" />
        <input type='hidden' name = 'user_id' value="<?php echo $id; ?>" />
        <input class="btn btn-success btn-flat" type="submit" value="ADD"/>
      </div>

    </form>


<script src="<?php echo base_url() ?>js/jquery.min.js"></script>
<script>

$(".payment_form").hide();
$(document).ready(function(){
    var select = $('<div class = "user-detail"><label><span>Type :</span><select name="pay_mode" id = "pay_mode"/></label></div>');
    var option = { 'm0': '---Select----', 'm1': 'Cash', 'm2': 'Net Banking', 'm3': 'Cheque/DD' }
    
    // $("#fillData").append(" <h5 class='user-detail'> Mode OF Payment : </h5>");
    $("#fillData").append(select);
    for(var val in option)
    {
      $('<option />', {value: val, text: option[val]}).appendTo($('#pay_mode'));
    }
    $("#pay_mode").change( function(e){
       var pay_mode = $("#pay_mode option:selected").val();
       $(".payment_form").hide();
       switch(pay_mode){
        
        case 'm1':
            $("#cash").show();
            $("#add_button").show();
            break;
        
        case 'm2':
            $("#net_bank").show();
            $("#add_button").show();
            break;
        
        case 'm3':
            $("#cheque").show();
            $("#add_button").show();
            break;
       }
    });
});


</script>
</aside>