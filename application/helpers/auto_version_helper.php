<?php

function auto_version($file)
  {
    // $staticPath = $this->config->item('static_path');
    $staticPath = config_item('static_path'); 
    if(strpos($file, '/') !== 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file))
      return $file;

    $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] ."/". $file);
    return $staticPath.preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
  }

?>