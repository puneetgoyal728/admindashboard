<?php
// include_once 'scrypt.php';
Class User extends CI_Model
{
  function __construct()
  {
    $this->load->helper('scrypt');
  }

  function add_user($email_id, $password, $name, $mobile, $company)
  {
    $pwd = create_hash($password);
    $row_ins = array(
      'email_id' => $email_id,
      'pwd' => $pwd,
      'userlevel' => 1,
      'name' => $name,
      'mobile' => $mobile,
      'company' => $company,
    );

    $this->db->set('rc_add_date', 'CURDATE()', FALSE);
    $this->db->set('rc_add_time', 'CURTIME()', FALSE);

    $this->db->trans_start();
    $this->db->insert('tbl_user_master', $row_ins);
    $user_id = $this->db->insert_id();
    $retVal = $this->add_user_log($row_ins, $user_id);
    $this->db->trans_complete();

    if ($retVal) {
      return $user_id;
    }
    return $retVal;
  }

  function add_user_log($row_ins, $user_id)
  {
    $this->db->set('usr_id', $user_id);
    $this->db->set('rc_add_date', 'CURDATE()', FALSE);
    $this->db->set('rc_add_time', 'CURTIME()', FALSE);
    $this->db->insert('tbl_user_master_log', $row_ins);
    return TRUE;
  }


  function edit_user($user_id, $name, $mobile, $company)
  {
    $row_updt = array(
      'name' => $name,
      'mobile' => $mobile,
      'company' => $company,
    );
    
    $this->db->trans_start();
    $this->db->set('rc_modi_date', 'CURDATE()', FALSE);
    $this->db->set('rc_modi_time', 'CURTIME()', FALSE);
    $this->db->where('usr_id', $user_id);
    $this->db->update('tbl_user_master', $row_updt);
    $retVal = $this->add_user_log($row_updt, $user_id);
    $this->db->trans_complete();
   
    return $retVal;
  }

 
 function fetch_details($email)
 {
   $this -> db -> select('usr_id, name');
   $this -> db -> from('tbl_user_master');
   $this -> db -> where('email_id', $email);
 
   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 function change_userlevel($user_id, $level)
 {
   $row_updt = array( 'userlevel' => $level, );
    
    $this->db->set('rc_modi_date', 'CURDATE()', FALSE);
    $this->db->set('rc_modi_time', 'CURTIME()', FALSE);
    $this->db->where('usr_id', $user_id);
    $this->db->update('tbl_user_master', $row_updt);
    return TRUE;
 }


 function fetch_last_user()
 {
   // SELECT tual.tual_id, tum.email_id, tum.name, tual.activity, tual.sub_activity from tbl_user_activity_log tual INNER JOIN tbl_user_master tum ON tum.usr_id=tual.user_id order by tual.tual_id DESC LIMIT 1;
   
   $this -> db -> select('tbl_user_activity_log.tual_id, tbl_user_activity_log.activity, tbl_user_activity_log.sub_activity, tbl_user_activity_log.rec_add_date,  tbl_user_activity_log.rec_add_time, tbl_user_master.name, tbl_user_master.usr_id, tbl_user_master.email_id');
   $this -> db -> from('tbl_user_activity_log');
   $this -> db -> join('tbl_user_master', 'tbl_user_master.usr_id = tbl_user_activity_log.user_id', 'inner');
   $this -> db -> order_by('tbl_user_activity_log.tual_id', "desc"); 
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }

 }


 function fetch_all_users()
 {
   $query = $this -> db -> query("SELECT tual.user_id, tual.activity, tual.rec_add_time, tual.rec_add_date, tum.name, tum.email_id, tum.userlevel FROM tbl_user_activity_log tual JOIN tbl_user_master tum ON tual.user_id = tum.usr_id WHERE (tual.tual_id,tual.user_id) IN (SELECT MAX(tual_id), user_id FROM tbl_user_activity_log GROUP BY user_id) ORDER BY tual.tual_id DESC");
   
   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }


 function list_cli_email()
 {
   $this -> db -> select('email_id');
   $this -> db -> from('tbl_user_master');
   $query = $this -> db -> get();

   if($query -> num_rows() > 0)
   {
     // echo $query->result();
     return $query->result();
   }
   else
   {
     return false;
   }
   
 }

 function fetch_user_detail($id)
 {
   $this -> db -> select('usr_id, name, email_id, mobile, company, userlevel');
   $this -> db -> from('tbl_user_master');
   $this -> db -> where('usr_id', $id);
   // echo "id :  ". $id;
   $query = $this -> db -> get();

   if($query -> num_rows() > 0)
   {  
     // echo $query->result();
     return $query->result();
   }
   else
   {
     return false;
   }
 }


 function all_user_detail()
 {
   $this -> db -> select('usr_id, name, email_id, mobile, company, userlevel');
   $this -> db -> from('tbl_user_master');
   
   $query = $this -> db -> get();

   if($query -> num_rows() > 0)
   {  
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 function list_all_activity($id)
 {
   $this -> db -> select('ip_addr, activity, sub_activity, rec_add_date as date, rec_add_time as time');
   $this -> db -> from('tbl_user_activity_log');
   $this -> db -> where('user_id', $id);
   $this -> db -> order_by('tual_id', "desc"); 


   $query = $this -> db -> get();

   if($query -> num_rows() > 0)
   {  
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 function add_user_activity_log($tual)
 {

    $this->db->set('rec_add_date', 'CURDATE()', FALSE);
    $this->db->set('rec_add_time', 'CURTIME()', FALSE);
    $this->db->insert('tbl_user_activity_log', $tual);
    return TRUE;
 }

}

?>