<?php 
// include_once 'scrypt.php';

Class Admin extends CI_Model
{
  
 function __construct()
 {
   $this->load->helper('scrypt');
 }

 function login($username, $password)
 {
    // echo "BC11 !";
    $this -> db -> select('id, username, password, access_level');
    $this -> db -> from('tbl_admin_master');
    $this -> db -> where('username', $username);
    // $this -> db -> limit(1);

    $query = $this -> db -> get();
  
    if($query -> num_rows() == 1)
    {
      foreach($query->result() as $row)
      {
       if (check($password, $row->password))
       {
         return $query->result();
       }
      }
    }
    return FALSE;
 }


 
  function new_admin($email_id, $password, $name, $accesslevel)
  {
    $pwd = create_hash($password);
    $row_ins = array(
      'email' => $email_id,
      'password' => $pwd,
      'access_level' => $accesslevel,
      'username' => $name,
    );
    
    $this->db->insert('tbl_admin_master', $row_ins);

    return TRUE;
  }



}

?>