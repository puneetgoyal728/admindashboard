<?php 
// include_once 'scrypt.php';

Class Payment extends CI_Model
{
 function __construct()
 {
   $this->load->helper('scrypt');
 }

 function fetch_last_pay()
 {
   // select tcwl.user_id, tum.name, tcwl.amount, tcwl.rec_add_date, tcwl.rec_add_time from tbl_client_wallet_log tcwl 
   // INNER join tbl_user_master tum ON tum.usr_id = tcwl.user_id  order by tcwl_id desc limit 1;
   $this -> db -> select('tbl_user_master.name, tbl_user_master.usr_id, tbl_client_wallet_log.amount, tbl_client_wallet_log.medium, tbl_client_wallet_log.rec_add_date, tbl_client_wallet_log.rec_add_time');
   $this -> db -> from('tbl_client_wallet_log');
   $this -> db -> join('tbl_user_master', 'tbl_user_master.usr_id = tbl_client_wallet_log.user_id', 'inner');
   $this -> db -> order_by('tbl_client_wallet_log.tcwl_id', "desc"); 
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }

 }


 function fetch_pay_graph_in()
 {
  // select count(amount), sum(amount), rec_add_date from tbl_client_wallet_log group by(rec_add_date);
  // select sum(amount), RecAddDate from tbl_crowd_wallet_log group by (RecAddDate);
   $this -> db -> select('sum(amount) as amount, rec_add_date as date ');
   $this -> db -> from('tbl_client_wallet_log');
   $this -> db -> group_by('rec_add_date');
   $this -> db -> order_by('rec_add_date', "asc");
 
   $query = $this -> db -> get();
   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return array();
   }
 }


 function fetch_pay_graph_out()
 {
  // select sum(amount), rec_add_date from tbl_client_wallet_log group by(rec_add_date);
  // select sum(amount), RecAddDate from tbl_crowd_wallet_log group by (RecAddDate);
   $this -> db -> select('sum(amount) as amount, RecAddDate as date ');
   $this -> db -> from('tbl_crowd_wallet_log');
   $this -> db -> group_by('RecAddDate');
   $this -> db -> order_by('RecAddDate', "asc");
 
   $query = $this -> db -> get();
   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return array();
   }
 }


 function fetch_all_pay()
 {
   $this -> db -> select('tbl_user_master.name, tbl_user_master.usr_id, tbl_client_wallet_log.amount, tbl_client_wallet_log.medium, tbl_client_wallet_log.rec_add_date, tbl_client_wallet_log.rec_add_time');
   $this -> db -> from('tbl_client_wallet_log');
   $this -> db -> join('tbl_user_master', 'tbl_user_master.usr_id = tbl_client_wallet_log.user_id', 'inner');
   $this -> db -> order_by('tbl_client_wallet_log.tcwl_id', "desc");
 
   $query = $this -> db -> get();
   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

 function fetch_pending()
 {
   // select sum(tbl_postpaid_log.amount), tbl_postpaid_log.user_id, max(tbl_postpaid_log.rec_add_date), 
  // max(tbl_postpaid_log.rec_add_time) from tbl_postpaid_log inner join tbl_user_master on 
  // tbl_postpaid_log.user_id = tbl_user_master.usr_id group by tbl_postpaid_log.user_id;

   $this -> db -> select('tbl_user_master.name, tbl_pending_wallet.curr_pend_amt as amount, tbl_pending_wallet.user_id, tbl_pending_wallet.last_tran_date as date, tbl_pending_wallet.last_tran_time as time');
   $this -> db -> from('tbl_pending_wallet');
   $this -> db -> join('tbl_user_master', 'tbl_user_master.usr_id = tbl_pending_wallet.user_id', 'inner');

   $this -> db -> where('tbl_pending_wallet.curr_pend_amt >',0);
   $query = $this -> db -> get();

   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {     
     return false;
   }

 }


 function add_payment_cash($amount, $user_id, $form_type, $amount_pp, $added_by)
 {
    $uniq_id = $this->generateID('tbl_cash_log');

    $mode = array(
      'uniq_id' =>  $uniq_id,
      'mode' => "Cash Transfer",
    );
    $row_ins = array();
    
    // echo $row_ins;
    $this->db->set('uniq_id', $uniq_id);
    $this->db->set('user_id', $user_id);
    $this->db->set('amount', $amount);
    $this->db->set('rec_add_date', 'CURDATE()', FALSE);
    $this->db->set('rec_add_time', 'CURTIME()', FALSE);
    $this->db->set('added_by', $added_by);
    $medium = 1; //medium 1 means Cash Transfer
    $trans_detail = json_encode($mode);
    
    $this->db->trans_start();
    $this->db->insert('tbl_cash_log', $row_ins);
    $trans_id = $this->db->insert_id();
    
    if ($form_type == 'add_pay') {
      $retVal = $this->insertWalletLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by);
    }elseif ($form_type == 'settle_pp') {
      $nett_amt = $amount - $amount_pp;
      if ($nett_amt <= 0) {
        $retVal = $this->insertSettleLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by);
      }
      else{
        $retVal = $this->insertSettleLog($user_id, $amount_pp, $trans_id, $trans_detail, $medium, $added_by);
        $ret = $this->insertWalletLog($user_id, $nett_amt, $trans_id, $trans_detail, $medium, $added_by);
        $retVal = $retVal and $ret;
      }
    }
    else {
      $retVal = FALSE;
    }
    $this->db->trans_complete();

    return $retVal;
 }



 function add_postpaid($amount, $user_id, $added_by)
 {
   $uniq_id = $this->generateID('tbl_postpaid_log');

   $mode = array(
      'uniq_id' =>  $uniq_id,
      'mode' => "Post Paid Credit",
    );
    $row_ins = array();
    
    // echo $row_ins;
    $this->db->set('uniq_id', $uniq_id);
    $this->db->set('user_id', $user_id);
    $this->db->set('amount', $amount);
    $this->db->set('rec_add_date', 'CURDATE()', FALSE);
    $this->db->set('rec_add_time', 'CURTIME()', FALSE);
    $this->db->set('added_by', $added_by);
    $medium = 10; //medium 10 means PostPaid
    $trans_detail = json_encode($mode);
    
    $this->db->trans_start();
    $this->db->insert('tbl_postpaid_log', $row_ins);
    $trans_id = $this->db->insert_id();    
    $retVal = $this->insertWalletLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by);
    $ret = $this->updatePendingWallet($user_id, $amount);
    $this->db->trans_complete();

    return ($retVal and $ret);
 }


 function add_free_credit($credit, $user_id, $limit, $added_by)
 {
    $uniq_id = $this->generateID('tbl_freecredit_log');

    if ($credit > $limit) {
      $credit = $limit; 
    }
    $mode = array(
      'uniq_id' =>  $uniq_id,
      'mode' => "Free Credit Added",
    );
    $medium = 5; //medium 5 means Free Credit Added
    $trans_detail = json_encode($mode);
    $row_ins = array();

    $this->db->trans_start();
    $this->db->set('uniq_id', $uniq_id);
    $this->db->set('user_id', $user_id);
    $this->db->set('credit', $credit);
    $this->db->set('rec_add_date', 'CURDATE()', FALSE);
    $this->db->set('rec_add_time', 'CURTIME()', FALSE);
    $this->db->set('added_by', $added_by);
    $this->db->insert('tbl_freecredit_log', $row_ins);
    $trans_id = $this->db->insert_id();
    $retVal = $this->insertWalletLog($user_id, $credit, $trans_id, $trans_detail, $medium, $added_by);
    $this->db->trans_complete();
   
    return $retVal;
 }


 function add_payment_net($user_id, $amount, $bnk_tid, $acc_name, $acc_no, $bnk_name, $branch, $ifsc, $form_type, $amount_pp, $added_by)
 {

   $uniq_id = $this->generateID('tbl_net_bank_log');
   $row_ins = array(
     'bank_trans_id' => $bnk_tid,
     'bank_name' => $bnk_name,
     'bank_branch' => $branch,
     'account_name' => $acc_name, 
     'ifsc_code' => $ifsc,
     'account_no' => $acc_no,
     'uniq_id' =>  $uniq_id,
    );
    $trans_det = array_merge($row_ins,array('mode'=>'Cheque/DD'));
    // $this->db->set('uniq_id', $uniq_id);
    $this->db->set('user_id', $user_id);
    $this->db->set('amount', $amount);
    $this->db->set('rec_add_date', 'CURDATE()', FALSE);
    $this->db->set('rec_add_time', 'CURTIME()', FALSE);
    $this->db->set('added_by', $added_by);
    $medium = 2; //medium 2 means Net Banking
    $trans_detail = json_encode($trans_det);
    
    $this->db->trans_start();
    $this->db->insert('tbl_net_bank_log', $row_ins);
    $trans_id = $this->db->insert_id();
    if ($form_type == 'add_pay') 
    {
      $retVal = $this->insertWalletLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by);
    }
    elseif ($form_type == 'settle_pp') 
    {
      $nett_amt = $amount - $amount_pp;
      if ($nett_amt <= 0) {
        $retVal = $this->insertSettleLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by);
      }
      else{
        $retVal = $this->insertSettleLog($user_id, $amount_pp, $trans_id, $trans_detail, $medium, $added_by);
        $ret = $this->insertWalletLog($user_id, $nett_amt, $trans_id, $trans_detail, $medium, $added_by);
        $retVal = $retVal and $ret;
      }
    }else {
      $retVal = FALSE;
    }
    $this->db->trans_complete();
    
    return $retVal;

 }


 function add_payment_dd($user_id, $amount, $cheq_no, $cheq_date, $acc_name, $bnk_name, $branch, $form_type, $amount_pp, $added_by)
 {

   $uniq_id = $this->generateID('tbl_cheque_log');
   $row_ins = array(
     'cheque_no' => $cheq_no,
     'cheque_date' => $cheq_date,
     'bank_name' => $bnk_name,
     'bank_branch' => $branch,
     'account_name' => $acc_name, 
     'uniq_id' => $uniq_id
    );
    $trans_det = array_merge($row_ins,array('mode'=>'Cheque/DD'));
    // $this->db->set('uniq_id', $uniq_id);
    $this->db->set('user_id', $user_id);
    $this->db->set('amount', $amount);
    $this->db->set('rec_add_date', 'CURDATE()', FALSE);
    $this->db->set('rec_add_time', 'CURTIME()', FALSE);
    $this->db->set('added_by', $added_by);
    $medium = 3; //medium 3 means Cheque/DD
    $trans_detail = json_encode($trans_det);
    
    $this->db->trans_start();
    $this->db->insert('tbl_cheque_log', $row_ins);
    $trans_id = $this->db->insert_id();
    if ($form_type == 'add_pay') 
    {
      $retVal = $this->insertWalletLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by);
    }
    elseif ($form_type == 'settle_pp') 
    {
      $nett_amt = $amount - $amount_pp;
      if ($nett_amt <= 0) {
        $retVal = $this->insertSettleLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by);
      }
      else{
        $retVal = $this->insertSettleLog($user_id, $amount_pp, $trans_id, $trans_detail, $medium, $added_by);
        $ret = $this->insertWalletLog($user_id, $nett_amt, $trans_id, $trans_detail, $medium, $added_by);
        $retVal = $retVal and $ret;
      }
    }else {
      $retVal = FALSE;
    }
    $this->db->trans_complete();
    
    return $retVal;
 }


 function generateID($tablename)
 {
   $saltFound = false;
   while (!$saltFound) {
       $salt = generateSalt(12);
       // echo "  salt : ", $salt;
       $this -> db -> select('id');
       $this -> db -> from($tablename);
       $this -> db -> where('uniq_id', $salt);
       
       $query = $this -> db -> get();
       if($query -> num_rows() == 0)
       {
         $saltFound = true;
       }
    }
   return $salt;
 }


 function insertWalletLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by)
 {
  $this -> db -> select('amount');
   $this -> db -> from('tbl_client_wallet');
   $this -> db -> where('user_id', $user_id);
   $query = $this -> db -> get();
   $new_amount=$amount;
    foreach($query->result() as $row)
     {
       $new_amount = $row->amount + $amount;
     }
     
    $row_ins = array(
     'user_id' => $user_id,
     'amount' => $amount,
     'type' => 'Credit',
     'trans_id' => $trans_id,
     'trans_details' => $trans_detail,
     'medium' => $medium, 
     'net_balance'=>$new_amount,
    );

    $this->db->set('rec_add_date', 'CURDATE()', FALSE);
    $this->db->set('rec_add_time', 'CURTIME()', FALSE);
    $this->db->set('added_by', $added_by);
    $this->db->insert('tbl_client_wallet_log', $row_ins);
    $retVal = $this->updateCliWallet($user_id, $amount);
    return $retVal; 
 }



 function updateCliWallet($user_id, $amount)
 {
   $this -> db -> select('amount');
   $this -> db -> from('tbl_client_wallet');
   $this -> db -> where('user_id', $user_id);
   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     foreach($query->result() as $row)
     {
       $new_amount = $row->amount + $amount;
     }
     $row_updt = array(
      'amount' => $new_amount,
      'last_tran_amt' => $amount,
      );
     $this->db->set('last_tran_date', 'CURDATE()', FALSE);
     $this->db->set('last_tran_time', 'CURTIME()', FALSE);
     $this->db->where('user_id', $user_id);
     $this->db->update('tbl_client_wallet', $row_updt); 
     return TRUE;           
   }
   else 
   {
    $this->addClientWallet($user_id, $amount);
    return TRUE;
   }

 }

 function addClientWallet($user_id, $amount)
 { 
   $row_ins = array(
    'user_id' => $user_id,
    'amount' => $amount ,
    'last_tran_amt' => $amount,
    );
   
   $this->db->set('last_tran_date', 'CURDATE()', FALSE);
   $this->db->set('last_tran_time', 'CURTIME()', FALSE);
   $this->db->insert('tbl_client_wallet', $row_ins);
   return TRUE;
 }


 function updatePendingWallet($user_id, $amount)
 {
   $this -> db -> select('curr_pend_amt');
   $this -> db -> from('tbl_pending_wallet');
   $this -> db -> where('user_id', $user_id);
   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     foreach($query->result() as $row)
     {
       $new_amount = $row->curr_pend_amt + $amount;
     }
     $row_updt = array(
      'curr_pend_amt' => $new_amount,
      'last_tran_amt' => $amount,
      );
     $this->db->set('last_tran_date', 'CURDATE()', FALSE);
     $this->db->set('last_tran_time', 'CURTIME()', FALSE);
     $this->db->where('user_id', $user_id);
     $this->db->update('tbl_pending_wallet', $row_updt); 
     return TRUE;
   }
   elseif ($query -> num_rows() == 0) {
      $row_ins = array();
      $this->db->set('user_id', $user_id);
      $this->db->set('curr_pend_amt', $amount);
      $this->db->set('last_tran_amt', $amount);
      $this->db->set('last_tran_date', 'CURDATE()', FALSE);
      $this->db->set('last_tran_time', 'CURTIME()', FALSE);
      $this->db->insert('tbl_pending_wallet', $row_ins);
      return TRUE; 
    } 
   else
   {
    return FALSE;
   }

 }


 function insertSettleLog($user_id, $amount, $trans_id, $trans_detail, $medium, $added_by)
 {
    $row_ins = array(
     'user_id' => $user_id,
     'amount' => $amount,
     'trans_id' => $trans_id,
     'trans_details' => $trans_detail,
     'medium' => $medium, 
    );

    $this->db->set('rec_add_date', 'CURDATE()', FALSE);
    $this->db->set('rec_add_time', 'CURTIME()', FALSE);
    $this->db->set('added_by', $added_by);
    $this->db->insert('tbl_pp_settle_log', $row_ins);
    $amount = -$amount;
    $retVal = $this->updatePendingWallet($user_id, $amount);
    return $retVal; 
 }

 function calFClimit($user_id, $month, $year)
 {
  // http://stackoverflow.com/questions/9104704/select-mysql-based-only-on-month-and-year
  // SELECT * FROM projects WHERE YEAR(Date) = 2011 AND MONTH(Date) = 5"
   $this -> db -> select('credit');
   $this -> db -> from('tbl_freecredit_log');
   $this -> db -> where('user_id', $user_id);
   $this -> db -> where('MONTH(rec_add_date)', $month, FALSE);
   $this -> db -> where('YEAR(rec_add_date)', $year, FALSE);
   $query = $this -> db -> get();

   $new_amount = 0;
   if($query -> num_rows() > 0)
   {
     foreach($query->result() as $row)
     {
       $new_amount += $row->credit;
     }
     return $new_amount;
   }
   else {
     return $new_amount;
   }
 }


 function FCsignupCheck($user_id)
 {
   $this -> db -> select('credit, added_by');
   $this -> db -> from('tbl_freecredit_log');
   $this -> db -> where('user_id', $user_id);
   
   $query = $this -> db -> get();
   if($query -> num_rows() > 0)
   {
     return false;
   }
   else {
     return true;
   }
 }




}
?>
