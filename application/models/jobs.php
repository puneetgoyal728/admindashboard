<?php 

Class Jobs extends CI_Model
{
 
 function fetch_last_job()
 {
   $this -> db -> select('tbl_job_master.usr_id, tbl_job_master.job_id, tbl_job_master.job_name, tbl_job_master.no_pages, tbl_job_master.recadd_date, tbl_job_master.recadd_time, tbl_user_master.name, tbl_user_master.email_id');
   $this -> db -> from('tbl_job_master');
   $this -> db -> join('tbl_user_master', 'tbl_user_master.usr_id = tbl_job_master.usr_id', 'inner');
   $this -> db -> order_by('tbl_job_master.job_id', "desc"); 
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return FALSE;
   }
 }


 function fetch_all_jobs_pending()
 {
   $query = $this -> db -> query("SELECT tjm.form_id, tjm.job_id, tjm.job_name, tjm.usr_id, tum.name, tjm.recadd_date as add_date, tjm.recadd_time as add_time, tjm.complete_date as cdate, tjm.complete_time as ctime, sum(tsm.processed) as comp, count(tsm.processed) as total FROM tbl_job_master tjm JOIN tbl_user_master tum JOIN tbl_snippets_master tsm ON tjm.usr_id = tum.usr_id AND tjm.job_id = tsm.job_id WHERE tjm.completed = 0 GROUP BY tjm.job_id ORDER BY tjm.job_id;");
   
   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return FALSE;
   }
 }

 function fetch_all_jobs_completed()
 {
   
   $query = $this -> db -> query("SELECT tjm.form_id, tjm.job_id, tjm.job_name, tjm.usr_id, tum.name, tjm.recadd_date as add_date, tjm.recadd_time as add_time, tjm.complete_date as cdate, tjm.complete_time as ctime, sum(tsm.processed) as comp, count(tsm.processed) as total FROM tbl_job_master tjm JOIN tbl_user_master tum JOIN tbl_snippets_master tsm ON tjm.usr_id = tum.usr_id AND tjm.job_id = tsm.job_id WHERE tjm.completed = 1 GROUP BY tjm.job_id ORDER BY tjm.job_id;");
   
   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return FALSE;
   }
 }


 function fetch_user_jobs($id)
 {
  // select * from tbl_image_master where user_id=2 group by(form_id);

   $this -> db -> select('job_name, no_pages, completed, active, form_id, recadd_date, recadd_time');
   $this -> db -> from('tbl_job_master');
   $this -> db -> where('usr_id', $id);
   $this -> db -> order_by('job_id', 'desc');
   $query = $this -> db -> get();

   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return FALSE;
   }
 }

 function fetch_user_temp($id)
 {
  // SELECT ttd.name name, tim.form_id form_id, count(tim.page_no) no_page, ttd.rc_add_time time, ttd.rc_add_date date from tbl_image_master tim join tbl_template_details ttd ON tim.form_id=ttd.form_id  where tim.user_id=5 group by (form_id)

   $this -> db -> select('tbl_template_details.name name, tbl_image_master.form_id form_id, count(tbl_image_master.page_no) no_page, tbl_template_details.rc_add_time time, tbl_template_details.rc_add_date date');
   $this -> db -> from('tbl_image_master');
   $this -> db -> join('tbl_template_details', 'tbl_image_master.form_id = tbl_template_details.form_id', 'inner');
   $this -> db -> where('tbl_image_master.user_id', $id);
   $this -> db -> group_by('tbl_image_master.form_id');
   $this -> db -> order_by('tbl_template_details.rc_modi_date', 'desc'); 
   $query = $this -> db -> get();

   if($query -> num_rows() > 0)
   {
     return $query->result();
   }
   else
   {
     return FALSE;
   }
 }


 function fetch_template($form_id, $page_no)
 {
    $this -> db -> select('data_img_name, min(page_no) page_no, width, height');
    $this -> db -> from('tbl_image_master');
    $this -> db -> where('form_id', $form_id);
    $this -> db -> where('active', 1);
    if ($page_no != NULL) {
      $this -> db -> where('page_no', $page_no);
    }
    // $this -> db -> order_by('page_no', 'asc');
    $query = $this -> db -> get();
    $count = $this->getCount($form_id);
    if($query -> num_rows() > 0) {
      return array('result' => $query->result(), 'count'=> $count);
    }
    else {
      return FALSE;
    }
 }

 function getCount($form_id)
 {
    $this -> db -> select('count(page_no) count');
    $this -> db -> from('tbl_image_master');
    $this -> db -> where('form_id', $form_id);
    $this -> db -> where('active', 1);
    $query = $this -> db -> get();
    if($query -> num_rows() > 0) {
      return $query->result();
    }
    else {
      return FALSE;
    }
 }

 function template_details($form_id, $page_no)
 {
    $this -> db -> select('field_id, field_cord, field_name, field_type, sub_field_type, field_details, instructions, dict_status');
    $this -> db -> from('tbl_template_master');
    $this -> db -> where('form_id', $form_id);
    $this -> db -> where('page_no', $page_no);
    $query = $this -> db -> get();

    if($query -> num_rows() > 0) {
      return $query->result();
    }
    else {
      return FALSE;
    }
 }


 function update_dict_status($form_id, $page_no, $field_id, $status)
 {
   $row_updt = array( 'dict_status' => $status);
   $this->db->where('form_id', $form_id);
   $this->db->where('page_no', $page_no);
   $this->db->where('field_id', $field_id);
   $this->db->update('tbl_template_master', $row_updt);
   if ($this->db->_error_message()) {
      return FALSE; // Or do whatever you gotta do here to raise an error
   } else {
      return $this->db->affected_rows();
   }
 }

}
?>
