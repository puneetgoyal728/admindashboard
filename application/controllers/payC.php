<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// include_once 'scrypt.php';
session_start(); //we need to call PHP's session object to access it through CI
class PayC extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('payment','',TRUE);
        $this->load->library('table');
        $this->load->helper('date');
        $this->load->helper('scrypt');
    }


    function list_all_pay()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            if ($session_data['accesslevel'] >2) {
                redirect('login', 'refresh');
            };

            $data['title'] = "All Pays !";
            $data['bar_active'] = "PAYS";
            $data['main_body'] = "pay_view";
            $data['recieved_pay'] = $this->recieved_pay();
            $data['pending_pay'] = $this->pending_pay();
            $this->load->view('template_view', $data);
        }
        else
        {
            redirect('login', 'refresh');
        }
    }


    function recieved_pay()
    {
        $session_data = $this->session->userdata('logged_in');
        if ($session_data['accesslevel'] >2) {
            redirect('login', 'refresh');
        };

        $result = $this->payment->fetch_all_pay();
        if($result)
        {
            $modes = array(
                    '1' => "Cash Transfer",
                    '2' => "Net Banking",
                    '3' => "Cheque/DD",
                    '5' => "Free Credits",
                    '10' => "Postpaid Credit",
            );
            foreach($result as $row)
            {
                $now = time();
                $post_date = strtotime($row->rec_add_date.$row->rec_add_time);
                $row->name = anchor(site_url(array('usersC', 'user_detail', $row->usr_id )), $row->name);
                $temp = array( $row->name, $row->amount, $modes[$row->medium], timespan($post_date, $now) . ' ago', );
                $this->table->add_row($temp);
            }
            $tmpl = array (
                    'table_open' => '<table class="table table-bordered">',
                    'table_close' => '</table>'
            );
            $this->table->set_template($tmpl);
            $this->table->set_heading('Client', 'Amount', 'Mode', 'Recieved time');
            // $this->table->set_caption('ALL Payment Details :');
            $retVal = $this->table->generate();
        }
        else
        {
            $retVal = $result;
        }
        return $retVal;
        // $this->load->view('pay_view', $data);

         
    }

    function pending_pay()
    {
        $session_data = $this->session->userdata('logged_in');
        if ($session_data['accesslevel'] >2) {
            redirect('login', 'refresh');
        }

        $result = $this->payment->fetch_pending();
        if($result) {

            foreach($result as $row) {
                // json_encode($row->user_id)
                $now = time();
                $post_date = strtotime($row->date.$row->time);
                $settle = anchor(site_url(array('payC', 'pp_page_show', urlencode($row->user_id), $row->name, $row->amount)), "add");
                $row->name = anchor(site_url(array('usersC', 'user_detail', $row->user_id )), $row->name);
                if ($session_data['accesslevel'] >1) {
                    $temp = array( $row->name, $row->amount, timespan($post_date, $now) . ' ago');
                }
                else {
                    $temp = array( $row->name, $row->amount, timespan($post_date, $now) . ' ago', $settle);
                }
                $this->table->add_row($temp);
            }
            $tmpl = array (
                    'table_open' => '<table class="table table-bordered">',
                    'table_close' => '</table>'
            );
            $this->table->set_template($tmpl);
            if ($session_data['accesslevel'] >1) {
                $this->table->set_heading('Client', 'Amount', 'Last Transaction');
            }
            else {
                $this->table->set_heading('Client', 'Amount', 'Last Transaction', 'Add Payment');
            }
            // $this->table->set_caption('Pending Payment Details :');
            $retVal = $this->table->generate();
        } else {
            $retVal = $result;
        }
        // $this->load->view('pending_pay_view', $data);
        return $retVal;

    }

    function pp_page_show($id, $n, $amount)
    {
        $name = urldecode($n);
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            if ($session_data['accesslevel'] >1) {
                redirect('login', 'refresh');
            };

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['id'] = $id;
            $data['name'] = $name;
            $data['amount'] = $amount;
            $data['bar_active'] = "PAYS";
            $data['title'] = "Add Pending Payment !";
            $data['main_body'] = "settle_pp_view";
            $this->load->view('template_view', $data);
            // $this->load->view('settle_pp_view', $data);
        }
        else
        {
            redirect('login', 'refresh');
        }
    }

    function settle_pp()
    {
        $session_data = $this->session->userdata('logged_in');
        if ($session_data['accesslevel'] >1) {
            redirect('login', 'refresh');
        };

        $form_type = $this->input->post('form_type');
        $amount_pp = $this->input->post('amount_pp');
        $this->add_payment($form_type, $amount_pp);
    }


    // public function tempFunc($user_id)
    // {
    //   // $hash = urldecode($h);
        //   // echo $hash;
        //   $added_by = "SIGN UP";
        //   // echo $added_by;
        //   // if (check( $user_id, $hash)) {
        //    // echo $added_by;
        //    $limit = $this->config->item('sign_up_FC');
        //    $checkDB = $this->payment->FCsignupCheck($user_id);
        //    if($checkDB){
        // 		$result = $this->payment->add_free_credit($limit, $user_id, $limit, $added_by);
            //    		// return $result;
            //    }
            //    // return FALSE;
            //   // }
            //   // else{
            //   	// echo "kch bhi ";
            //   	// return FALSE;
            //   // }
            //   // return TRUE;
            // }



            public function newUserWalletAPI()
            {
                $h=$_GET["hash"];
                $user_id=$_GET["user_id"];
                $hash = rawurldecode($h);
                $added_by = "self";
                if (check( $user_id, $hash)) {
                    $limit = $this->config->item('sign_up_FC');
                    $checkDB = $this->payment->FCsignupCheck($user_id);
                    if($checkDB){
                        $result = $this->payment->add_free_credit($limit, $user_id, $limit, $added_by);
                        return $result;
                    }
                }
                return FALSE;
            }




            function add_payment($form_type ='add_pay', $amount_pp = 0)
            {
                $session_data = $this->session->userdata('logged_in');
                if ($session_data['accesslevel'] >1) {
                    redirect('login', 'refresh');
                };

                $pay_mode = $this->input->post('pay_mode');
                $user_id = $this->input->post('user_id');
                if($this->session->userdata('logged_in')) {
                    $session_data = $this->session->userdata('logged_in');
                    $added_by = $session_data['id'];
                }
                else {
                    // as of now, this must not occur.
                    $added_by = "S-B";
                }

                switch($pay_mode){
                    case 'm1':
                        $amount = abs($this->input->post('cash_amount'));
                        $result = $this->payment->add_payment_cash($amount, $user_id, $form_type, $amount_pp, $added_by);
                        break;
                    case 'm2':
                        $amount = abs($this->input->post('net_amount'));
                        $bnk_tid = $this->input->post('net_bnk_tid');
                        $acc_name = $this->input->post('net_acc_name');
                        $acc_no = $this->input->post('net_acc_no');
                        $bnk_name = $this->input->post('net_bnk_name');
                        $branch = $this->input->post('net_bnk_branch');
                        $ifsc = $this->input->post('net_ifsc_code');
                        $result = $this->payment->add_payment_net($user_id, $amount, $bnk_tid, $acc_name, $acc_no, $bnk_name, $branch, $ifsc, $form_type, $amount_pp, $added_by);
                        break;
                    case 'm3':
                        $amount = abs($this->input->post('dd_amount'));
                        $cheq_no = $this->input->post('dd_cheq_no');
                        $cheq_date = $this->input->post('dd_cheq_date');
                        $acc_name = $this->input->post('dd_acc_name');
                        $bnk_name = $this->input->post('dd_bnk_name');
                        $branch = $this->input->post('dd_bnk_branch');
                        $result = $this->payment->add_payment_dd($user_id, $amount, $cheq_no, $cheq_date, $acc_name, $bnk_name, $branch, $form_type, $amount_pp, $added_by);
                        break;
                    case 'm4':
                        $amount = abs($this->input->post('cash_amount'));
                        $result = $this->payment->add_postpaid($amount, $user_id, $added_by);
                        break;
                    case 'm5':
                        $credit = abs($this->input->post('cash_amount'));
                        $month = date('m', time());
                        $year = date('Y', time());
                        $alloted = $this->payment->calFClimit($user_id, $month, $year);
                        $limit = $this->config->item('FC_limit');
                        $new_limit = $limit - $alloted;
                        if ($new_limit > 0) {
                            $result = $this->payment->add_free_credit($credit, $user_id, $new_limit, $added_by);
                        } else {
                            $result = FALSE;
                        }
                        break;

                }
                // echo $result;
                if ($result){
                    $this->payment_success($form_type);
                } else {
                    $this->payment_failed($form_type);
                }
            }


            function payment_success($form_type ='add_pay')
            {
                if($this->session->userdata('logged_in'))
                {
                    if ($form_type == 'settle_pp') {
                        redirect('payC/list_all_pay', 'refresh');
                    }
                    // the 101 in URL is the token as succesful pament add.
                    redirect('home/new_home/101', 'refresh');
                    // $this->load->view('home_new', $data);
                }
                else
                {
                    redirect('login', 'refresh');
                }
            }

            function payment_failed($form_type ='add_pay')
            {
                if($this->session->userdata('logged_in'))
                {
                    if ($form_type == 'settle_pp') {
                        redirect('payC/list_all_pay', 'refresh');
                    }
                    // the 102 in URL is the token as succesful pament add.
                    redirect('home/new_home/102', 'refresh');
                }
                else
                {
                    redirect('login', 'refresh');
                }
            }


            function load_graph()
            {
                $session_data = $this->session->userdata('logged_in');
                if ($session_data['accesslevel'] >2) {
                    echo json_encode(FALSE);
                };

                $result = $this->payment->fetch_pay_graph_in();
                $result_out = $this->payment->fetch_pay_graph_out();
                $inputDataArr = array();
                foreach ($result as $inpAmt){
                    $dateIdx = strtotime($inpAmt->date);
                    $inputDataArr[$dateIdx] = $inpAmt->amount;
                }
                $outputDataArr = array();
                foreach ($result_out as $outAmt) {
                    $dateIdx = strtotime($outAmt->date);
                    $outputDataArr[$dateIdx] = $outAmt->amount;
                }
                $data_array = array();
                $data_array_out = array();
                $now = time();
                // print_r($inputDataArr);
                // print_r($outputDataArr);exit;
                if(count($outputDataArr)>0 && count($inputDataArr)>0){
                    $outPutKeys = array_keys($outputDataArr);
                    $inputKeys = array_keys($inputDataArr);
                    $currdateIdx = min($outPutKeys[0],$inputKeys[0]);
                    $returnArr = array();
                    while($currdateIdx < $now){
                        $inVal  = (isset($inputDataArr[$currdateIdx])?$inputDataArr[$currdateIdx]:0);
                        $outVal = (isset($outputDataArr[$currdateIdx])?$outputDataArr[$currdateIdx]:0);
                        array_push($returnArr, array('date'=>date("Y-m-d", $currdateIdx),'in'=>$inVal,'out'=>$outVal));
                        $currdateIdx+=86400;
                    }

                    echo json_encode($returnArr);
                }
                echo false;
            }


    }

    ?>