<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class UsersC extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
   $this->load->library('table');
   $this->load->helper(array('scrypt', 'date'));
 }
 

 function list_all_users()
 {
   if($this->session->userdata('logged_in'))
   {
      $data['title'] = "Users !";
      $data['bar_active'] = "USERS";
      $data['main_body'] = "users_view"; 
      $data['all_activites'] = $this->all_user_activity();
      $data['all_details'] = $this->all_user_detail();
      $this->load->view('template_view', $data);
   }
   else
   {
      redirect('login', 'refresh');
   } 
   
 }


 function all_user_activity()
 {  
    // $this->load->helper('date');
    $result = $this->user->fetch_all_users();
    if($result)
    {
     foreach($result as $row)
     {
       $now = time();
       $post_date = strtotime($row->rec_add_date.$row->rec_add_time);
       $all_act = anchor(site_url(array('usersC', 'list_all_activity', urlencode($row->user_id), $row->name)), "all_activities");
       if ($row->userlevel == '0') {
          $row->name = anchor(site_url(array('usersC', 'user_detail', urlencode($row->user_id) )), $row->name, array('class' => 'deactive'));
       }
       elseif ($row->userlevel == '1') {
          $row->name = anchor(site_url(array('usersC', 'user_detail', urlencode($row->user_id) )), $row->name, array('class' => 'active'));
       }
       $temp = array( $row->name, $row->email_id, $row->activity, timespan($post_date, $now).' ago', $all_act, );
       $this->table->add_row($temp);
     }
     $tmpl = array (
        'table_open' => '<table class="table table-condensed">',
        'table_close' => '</table>'
      );
     $this->table->set_template($tmpl);
     $this->table->set_heading('Client', 'Email', 'Activity', 'When', "Action");

     // $this->table->set_caption('ALL User Ativities :');
     $retVal = $this->table->generate();
   } 
   else 
   {
    $retVal = $result;
   }
   // $data['title'] = "Users !";
   // $data['main_body'] = "users_view"; 
   // $this->load->view('template_view', $data);
   return $retVal ;
   
 }


 function all_user_detail()
 { 
    // $this->load->helper('date');
    $result = $this->user->all_user_detail();
    if($result)
    {
     foreach($result as $row)
     {
       if ($row->userlevel == '0') {
          $row->name = anchor(site_url(array('usersC', 'user_detail', urlencode($row->usr_id) )), $row->name, array('class' => 'deactive'));
          $userStatus = "Deactivated";
       }
       elseif ($row->userlevel == '1') {
          $row->name = anchor(site_url(array('usersC', 'user_detail', urlencode($row->usr_id) )), $row->name, array('class' => 'active'));
          $userStatus = "Active";
       }
       $temp = array( $row->name, $row->email_id,  $row->mobile, $row->company, );
       $this->table->add_row($temp);
     }
     $tmpl = array (
        'table_open' => '<table class="table table-condensed">',
        'table_close' => '</table>'
      );
     $this->table->set_template($tmpl);
     $this->table->set_heading('Client', 'Email', 'Contact No', 'Company');

     // $this->table->set_caption('ALL User Ativities :');
     $retVal = $this->table->generate();
   } 
   else 
   {
    $retVal = $result;
   }
   // $data['title'] = "Users !";
   // $data['main_body'] = "users_view"; 
   // $this->load->view('template_view', $data);
   return $retVal ;

 }


 function user_detail($id)
 {  
   $this->load->helper(array('form'));
   if($this->session->userdata('logged_in'))
   {  
     $result = $this->user->fetch_user_detail($id);

     if($result)
     {
       foreach($result as $row)
       {
         $data['userlevel'] = $row->userlevel;
         if ($row->userlevel == '0') {
          $userStatus = "Deactivated";
         }
         elseif ($row->userlevel == '1') {
            $userStatus = "Active";
         }
         // $data['status'] = $userStatus;
         $data_array = array( $row->name, $row->email_id, $row->mobile, $row->company, );
       }
       // $data['job_data'] = $data_array;
       $data['row_data'] = $data_array;
       $data['user_id'] = $id;
     } 
     $job_data = $this->fetch_user_jobs($id);
     $temp_data = $this->fetch_user_temp($id);
     $data['job_data'] = $job_data;
     $data['temp_data'] = $temp_data;
     $data['bar_active'] = "USERS";
     $data['title'] = "All Pays !";
     $data['main_body'] = "user_detail_view";
     $this->load->view('template_view', $data);
   }
   else
   {
     redirect('login', 'refresh');
   }

 }

 function fetch_user_jobs($id)
 {
   $this->load->model('jobs','',TRUE);
   // $this->load->helper('date');
   $result = $this->jobs->fetch_user_jobs($id);

   if($result)
     {
        foreach($result as $row)
        {
        //  select('job_name, no_pages, completed, form_id, recadd_date, recadd_time');
          $now = time();
          $post_date = strtotime($row->recadd_date.$row->recadd_time);
          if ($row->active == '1') {
            if ($row->completed == '0') {
               $jobStatus = '<span class="badge bg-light-blue" >Pending</span>';
             }
             elseif ($row->completed == '1') {
               $jobStatus = '<span class="badge bg-green" >Completed</span>';
             }
          }
          elseif ($row->active == '0') {
            $jobStatus = '<p class="badge bg-red" >Inactive</p>';
          }
          $view_temp = anchor(site_url(array('jobsC', 'show_template', $row->form_id)), "view");
          $temp = array( $row->job_name, $row->no_pages, $jobStatus, timespan($post_date, $now).' ago', $view_temp, );
          $this->table->add_row($temp);
        }
        $tmpl = array (
          'table_open' => '<table class="table table-condensed">',
          'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('Job Name', 'Pages', 'Status', 'Submitted', 'Template');
        // $this->table->set_caption('ALL Pending Job Details :');
        $retVal = $this->table->generate();

        return $retVal;
     }
   else
     {
        return "";
     }
// fetch_user_temp($id)
 }


 function fetch_user_temp($id)
 {
   $this->load->model('jobs','',TRUE);
   // $this->load->helper('date');
   $result = $this->jobs->fetch_user_temp($id);

   if($result)
     {
        foreach($result as $row)
        {
          $now = time();
          $post_date = strtotime($row->date.$row->time);
          $view_temp = anchor(site_url(array('jobsC', 'show_template', $row->form_id)), "view");
          $temp = array( $row->name, $row->no_page, timespan($post_date, $now).' ago', $view_temp, );
          $this->table->add_row($temp);
        }
        $tmpl = array (
          'table_open' => '<table class="table table-condensed">',
          'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('Template Name', 'Pages', 'Ceated', 'Template');
        // $this->table->set_caption('ALL Pending Job Details :');
        $retVal = $this->table->generate();

        return $retVal;
     }
   else
     {
        return "";
     }
 }




 function list_all_activity($id, $name)
 {
   if($this->session->userdata('logged_in'))
   {  
     // $this->load->helper('date');
     $result = $this->user->list_all_activity($id);

     if($result)
     {
       foreach($result as $row)
       { 
         $now = time();
         $post_date = strtotime($row->date.$row->time); 
         $temp = array( inet_ntop ( $row->ip_addr ), $row->activity, $row->sub_activity, timespan($post_date, $now).' ago', );
         // $temp = array( $row->name, $row->email_id, $row->activity, timespan($post_date, $now).' ago', $all_act, );
         $this->table->add_row($temp);
       }
       $tmpl = array (
          'table_open' => '<table class="table table-bordered">',
          'table_close' => '</table>'
        );
       $this->table->set_template($tmpl);
       $this->table->set_heading('IP', 'Activity', 'Sub Activity', 'When');

       // $this->table->set_caption('All activities for user_id : '.$id);
       $data['all_act'] = $this->table->generate();
     }
     $data['user_name'] = urldecode($name);
     $data['bar_active'] = "USERS";
     $data['title'] = "All Activity !";
     $data['main_body'] = "all_activity_view";
     $this->load->view('template_view', $data);
   }
   else
   {
     redirect('login', 'refresh');
   }
 }

 
 function fetch_details()
 {
   $email = $this->input->post('email');
   $result = $this->user->fetch_details($email);
   if($result)
   {
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->usr_id,
         'username' => $row->name
       );
     }
     $r= json_encode($sess_array);
     echo $r; 
   } else {
     echo $result;
   }

 }


 function change_userlevel()
 {
   $user_id = $this->input->post('user_id');
   $level = $this->input->post('level');
   if($level==1) {
      $result = $this->user->change_userlevel($user_id,0);
      echo "0";
   } else {
      $result = $this->user->change_userlevel($user_id,1);
      echo "1";
   }
 }


 function list_cli_email()
 { 
   $result = $this->user->list_cli_email();
   if($result)
   {
     $data_array = array();
     foreach($result as $row)
     {
       array_push($data_array, $row->email_id);
     }
     $r= json_encode($data_array);
     echo $r; 
   } else {
     echo $result;
   }
 }


 function add_user()
 {
    $session_data = $this->session->userdata('logged_in');
    if ($session_data['accesslevel'] >1) {
      redirect('login', 'refresh');
    };

    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
    $this->form_validation->set_rules('email_id', 'email_id', 'trim|required|valid_email|is_unique[tbl_user_master.email_id]');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
    // $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean');
    $this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required|min_length[6]|max_length[12]|xss_clean');
    $this->form_validation->set_rules('company', 'Company', 'trim|xss_clean');

    if($this->form_validation->run() == FALSE)
    {
      //Field validation failed.  User redirected to login page
      $data['bar_active'] = "ADDUSER";
      $data['title'] = "Sign Up !";
      $data['main_body'] = "add_user_view"; 
      $this->load->view('template_view', $data);
    }
    else
    {
      $this->load->model('payment','',TRUE);

      $email_id = $this->input->post('email_id');
      $password = $this->input->post('password');
      $name = $this->input->post('name');
      $mobile = $this->input->post('mobile_no');
      $company = $this->input->post('company');
      $added_by = "self";

      $user_id = $this->user->add_user($email_id, $password, $name, $mobile, $company);
      if ($user_id) {
        $result = $this->payment->addClientWallet($user_id, 0.0);
        if ($result) {
          $hash = create_hash($user_id);
          
          // $creditadded = $this->payC->newUserWalletAPI(urlencode($hash), $user_id);
          // redirect('payC/newUserWalletAPI/'.urlencode($hash).'/'.$user_id, 'refresh');
          // echo "something 2: ";
          // redirect('payC/tempFunc/'.$user_id, 'refresh');
          $limit = $this->config->item('sign_up_FC');
          $checkDB = $this->payment->FCsignupCheck($user_id);
          if($checkDB){
            $result = $this->payment->add_free_credit($limit, $user_id, $limit, $added_by);
            redirect('home/new_home/201', 'refresh');
          } else{
            redirect('home/new_home/401', 'refresh');
          }
        } else{
            redirect('home/new_home/402', 'refresh');
        }
      } else {
          redirect('home/new_home/202', 'refresh');
      }
    }
 }

 function edit_user()
 {
    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

    $this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required|numeric|min_length[6]|max_length[14]|xss_clean');
    $this->form_validation->set_rules('company', 'Company', 'trim|xss_clean');

    $user_id = $this->input->post('user_id');
    $email_id = $this->input->post('email_id');
    $name = $this->input->post('name');
    $mobile = $this->input->post('mobile_no');
    $company = $this->input->post('company');
    $row_data = $this->input->post('row_data');
    
    if($this->form_validation->run() == FALSE)
    {
      //Field validation failed.  User redirected to login page
      $data['reload'] = 1;
      $data['row_data'] = json_decode($row_data);
      $data['user_id'] = $user_id;
      $this->load->view('user_detail_view', $data);
    }
    else
    {
      $result = $this->user->edit_user($user_id, $name, $mobile, $company);
      if ($result)
      { 
        redirect('usersC/user_detail/'.$user_id, 'refresh');
      }
      else
      {
        echo $result;
      }
    }
 }

 public function addUserActivityLog()
 {
   // $user_id, $ip_addr, $activity, $sub_activity
      // http://stackoverflow.com/questions/6427786/ip-address-storing-in-mysql-database
      // http://daipratt.co.uk/mysql-store-ip-address/ 
      // inet_pton('192.168.1.1')
   $hash =$_GET["hash"];
   $user_id=$_GET["user_id"];
   $ip_addr=$_GET["ip"];
   $activity=$_GET["activity"];
   $sub_activity=$_GET["sub_activity"];
   $tual = array(
      'user_id' => $user_id,
      'ip_addr' => inet_pton($ip_addr),
      'activity'=> $activity,
      'sub_activity' => $sub_activity,     
    );
    echo $hash;
   if (check($user_id, $hash)) {
      $result = $this->user->add_user_activity_log($tual);
      return $result;
   } else {
      return FALSE;
   }
 }


}
 
?>