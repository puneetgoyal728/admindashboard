<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
 }
 
 function index()
 {
   // $this->output->enable_profiler(TRUE);  
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];

     $this->load->view('home_view', $data);
   }
   else
   {
     // TODO: http://stackoverflow.com/questions/21110197/how-to-call-one-controller-function-in-another-controller-in-codeigniter
     // TODO: http://philsturgeon.uk/blog/2010/02/CodeIgniter-Base-Classes-Keeping-it-DRY
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 


 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }



 function add_user()
 {
  if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     if ($session_data['accesslevel'] >1) {
        redirect('login', 'refresh');
     };

     $this->load->helper(array('form'));
     $data['title'] = "Sign Up !";
     $data['bar_active'] = "ADDUSER";
     $data['main_body'] = "add_user_view"; 
     $this->load->view('template_view', $data);
   }
   else
   {
     redirect('login', 'refresh');
   }
 }


 function add_admin()
 {
  if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     if ($session_data['accesslevel'] >1) {
        redirect('login', 'refresh');
     };
     $this->load->helper(array('form'));
     $data['title'] = "New Admin";
     $data['bar_active'] = "ADDADMIN";
     $data['main_body'] = "add_admin_view"; 
     $this->load->view('template_view', $data);
   }
   else
   {
     redirect('login', 'refresh');
   }
 }

 function new_admin()
 {
    $session_data = $this->session->userdata('logged_in');
    if ($session_data['accesslevel'] >1) {
      redirect('login', 'refresh');
    };
    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|is_unique[tbl_admin_master.username');
    $this->form_validation->set_rules('email_id', 'email_id', 'trim|required|valid_email]');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
    $this->form_validation->set_rules('accesslevel', 'Access Level', 'trim|required|xss_clean');

    if($this->form_validation->run() == FALSE)
    {
      //Field validation failed.  User redirected to login page
      $data['bar_active'] = "ADDADMIN";
      $data['title'] = "New Admin";
      $data['main_body'] = "add_admin_view"; 
      $this->load->view('template_view', $data);
      // $this->load->view('add_user_view');
    }
    else
    {
      $this->load->model('admin','',TRUE);

      $email_id = $this->input->post('email_id');
      $password = $this->input->post('password');
      $name = $this->input->post('name');
      $accesslevel = $this->input->post('accesslevel');

      $result = $this->admin->new_admin($email_id, $password, $name, $accesslevel);
      if ($result) {
        redirect('home/new_home/301', 'refresh');
      }
    }
 }



 function make_payment()
 {
   $session_data = $this->session->userdata('logged_in');
   if ($session_data['accesslevel'] >1) {
      redirect('login', 'refresh');
   };
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $data['title'] = "Add Payment !";
     $data['bar_active'] = "ADDPAY";
     $data['main_body'] = "add_payment_view"; 
     $this->load->view('template_view', $data);
   }
   else
   {
     redirect('login', 'refresh');
   }
 }



 function new_home($idx=0)
 {
   // $this->output->enable_profiler(TRUE);
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $data['last_job_box'] = $this->last_job();
     $data['last_user_box'] = $this->last_user();
     $data['last_pay_box'] = $this->last_pay();
     switch ($idx) {
       case 101:
         $data['msg'] = "Payment Added Sucessfully !!!";
         break;
       case 102:
           $data['msg'] = "Payment Failed !!!";
           break;
       case 201:
           $data['msg'] = "User Added Successfully !!!";
           break;
       case 202:
           $data['msg'] = "Add User Failed !!!";
           break;
       case 301:
           $data['msg'] = "New Admin Added Successfully !!!";
           break;
       case 401:
           $data['msg'] = "User added, Sign UP Free Credits Not Assigned !!!";
           break;
       case 402:
           $data['msg'] = "User added, Wallet Creation Failed !!!";
           break;        
       
       default:
         break;
     }
     $data['title'] = "Dashboard !";
     $data['bar_active'] = "HOME";
     $data['main_body'] = "home_new"; 
     $this->load->view('template_view', $data);
   }
   else
   {
     redirect('login', 'refresh');
   }
 }


 function last_pay()
 {  
   $session_data = $this->session->userdata('logged_in');
   if ($session_data['accesslevel'] >2) {
      return FALSE;
   };
   $this->load->model('payment','',TRUE);
   $this->load->helper('date');
   $result = $this->payment->fetch_last_pay();
   if($result)
   {
     $modes = array(
      '1' => "Cash Transfer",
      '2' => "Net Banking",
      '3' => "Cheque/DD",
      '5' => "Free Creits",
      '10' => "Postpaid Credit",
     ); 
     foreach($result as $row)
     {
       $now = time();
       $post_date = strtotime($row->rec_add_date.$row->rec_add_time);
       $row->name = anchor(site_url(array('usersC', 'user_detail', $row->usr_id )), $row->name);
        $data_array = array( 
        "Client" => $row->name,
        "Amount" =>$row->amount,
        "Mode" => $modes[$row->medium],
        "When" => timespan($post_date, $now).' ago', );
     }

     return $data_array;
   } 
   else 
   {
     return $result;
   }

 }


 function last_user()
 {
   $this->load->model('user','',TRUE);
   $this->load->helper('date');
   $result = $this->user->fetch_last_user();
   if($result)
   {
     foreach($result as $row)
     {
       $now = time();
       $post_date = strtotime($row->rec_add_date.$row->rec_add_time);
       $row->name = anchor(site_url(array('usersC', 'user_detail', $row->usr_id )), $row->name);  
       $data_array = array( 
        "Client" => $row->name,
        "Activity" => $row->activity,
        "When" => timespan($post_date, $now).' ago', );
     }
     
     return $data_array;
   } 
   else 
   {
     return $result;
   }

 }


 function last_job()
 {
   $this->load->model('jobs','',TRUE);
   $this->load->helper('date');
   $result = $this->jobs->fetch_last_job();
   if($result)
   {
     foreach($result as $row)
     {
       $now = time();
       $post_date = strtotime($row->recadd_date.$row->recadd_time);
       $row->name = anchor(site_url(array('usersC', 'user_detail', $row->usr_id )), $row->name);
       $data_array = array( 
        "Job_Name" =>$row->job_name,
        "Client" => $row->name,
        "Pages" => $row->no_pages,
        "When" => timespan($post_date, $now).' ago', );
     }
     return $data_array;
   } 
   else 
   {
     return $result;
   }

 }

}
 
?>