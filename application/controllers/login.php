<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Login extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin','',TRUE);
	}

	function index()
	{	
		if($this->session->userdata('logged_in'))
		{
			redirect('home/new_home', 'refresh');
		}
		else
		{
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}	
	}


	function verifyLogin()
	{
		//This method will have the credentials validation
		// There are numerous rules available which you can read about in the validation reference.
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed.  User redirected to login page
			$this->load->view('login_view');
		}
		else
		{
			//Go to private area
			// redirect('usersC/addUserActivityLog');
			redirect('home/new_home', 'refresh');
		}
	}

	function confirm_pp()
	{
		$this->load->library('form_validation');
		// TODO: Increase Security
		// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
		$password = $this->input->post('password');
		$res = $this->check_database($password);
		if($res)
		{
			echo True;
		}
		else
		{
			echo "Incorrect Password !";
		}
	}

	function check_database($password)
	{
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');
		$result = $this->admin->login($username, $password);
		// echo json_encode($result);
		if($result)
		{	
			foreach($result as $row)
			{
				$sess_array = array(
				 'id' => $row->id,
				 'username' => $row->username,
				 'accesslevel' => $row->access_level,
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			// exit();
			return FALSE;
		}
	}

}
 
?>