<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class JobsC extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('jobs','',TRUE);
        $this->load->library('table');
    }


    function list_all_jobs()
    {
        if($this->session->userdata('logged_in'))
        {
            $data['title'] = "Jobs !";
            $data['bar_active'] = "JOBS";
            $data['main_body'] = "jobs_view";
            $data['pending_jobs'] = $this->pending_jobs();
            $data['completed_jobs'] = $this->completed_jobs();
            $this->load->view('template_view', $data);
        }
        else
        {
            redirect('login', 'refresh');
        }
         
    }

    function pending_jobs()
    {
        $this->load->helper('date');
        $result = $this->jobs->fetch_all_jobs_pending();
        if($result)
        {
            foreach($result as $row)
            {
                $now = time();
                $post_date = strtotime($row->add_date.$row->add_time);
                $com_date = strtotime($row->cdate.$row->ctime);
                $view_temp = anchor(site_url(array('jobsC', 'show_template', $row->form_id)), "view");
                $row->name = anchor(site_url(array('usersC', 'user_detail', $row->usr_id )), $row->name);
                $temp = array( $row->job_id, $row->job_name, $row->name, $row->comp." / ".$row->total , timespan($post_date, $now).' ago', $view_temp, );
                $this->table->add_row($temp);
            }
            $tmpl = array (
                    'table_open' => '<table class="table table-condensed">',
                    'table_close' => '</table>'
            );
            $this->table->set_template($tmpl);
            $this->table->set_heading('Job ID', 'Job Name', 'Client', 'Snippets', 'Submitted', 'Template');
            // $this->table->set_caption('ALL Pending Job Details :');
            $retVal = $this->table->generate();
        }
        else
        {
            $retVal = $result;
        }
        return $retVal;
    }

    function completed_jobs()
    {
        $this->load->helper('date');
        $result = $this->jobs->fetch_all_jobs_completed();
        if($result)
        {
            foreach($result as $row)
            {
                $now = time();
                $post_date = strtotime($row->add_date.$row->add_time);
                $com_date = strtotime($row->cdate.$row->ctime);
                $view_temp = anchor(site_url(array('jobsC', 'show_template', $row->form_id)), "view");
                $row->name = anchor(site_url(array('usersC', 'user_detail', $row->usr_id )), $row->name);
                $temp = array( $row->job_id, $row->job_name, $row->name, $row->comp." / ".$row->total , timespan($post_date, $now).' ago', timespan($com_date, $now).' ago', $view_temp, );
                $this->table->add_row($temp);
            }
            $tmpl = array (
                    'table_open' => '<table class="table table-condensed">',
                    'table_close' => '</table>'
            );
            $this->table->set_template($tmpl);
            $this->table->set_heading('Job ID', 'Job Name', 'Client', 'Snippets', 'Submitted', 'Completed', 'Template');
            // $this->table->set_caption('ALL completed Job Details :');
            $retVal = $this->table->generate();
        }
        else
        {
            $retVal = $result;
        }
        return $retVal;
    }


    function show_template($form_id, $page_no=NULL)
    {
        if($this->session->userdata('logged_in'))
        {
            // TODO make sure this function doesn't get called everytime page is loaded - NOT possible.
            $this->load->library('pagination');
            $temp_path = $this->config->item('template_image_path');
            $result = $this->jobs->fetch_template($form_id, $page_no);
            if($result["result"]) {
                $data_array = array();
                foreach($result["result"] as $row) {
                    $page = $row->page_no;
                    $image = $row->data_img_name;
                    $width = $row->width;
                    $height = $row->height;
                    $details = array('page'=>$page, 'image'=>$temp_path.$image, 'width'=> $width, 'height'=> $height);
                    array_push($data_array, $details);
                }
                $retVal = $this->template_details($form_id, $details["page"]);
            }
            else {
                $retVal = "";
            }
            // echo "count : ";
            if($result["count"]) {
                foreach($result["count"] as $row) {
                    $config['total_rows'] = $row->count;
                    // echo json_encode($row->count);
                }
            }
            $config['base_url'] = base_url().'jobsC/show_template/'.$form_id.'/';
            $config['per_page'] = 1;
            $config['uri_segment'] = 4;
            $config['use_page_numbers'] = TRUE;
            $this->pagination->initialize($config);

            $data['form_id'] = $form_id;
            $data['img_details'] = $details;
            $data['temp_details'] = $retVal;
            $data['title'] = "Template";
            $data['bar_active'] = "JOBS";
            $data['main_body'] = "show_temp_view";
            $this->load->view('template_view', $data);
        }
        else
        {
            redirect('login', 'refresh');
        }

    }

    function template_details($form_id, $page_no)
    {
        $result = $this->jobs->template_details($form_id, $page_no);
        $temp_det = array();
        if($result) {
            $data_array = array();
            foreach($result as $row) {
                $data_det = array(
                        "id"=>$row->field_id,
                        "coordinates"=>$row->field_cord,
                        "name"=>$row->field_name,
                        "type"=>$row->field_type,
                        "sub_type"=>$row->sub_field_type,
                        "details"=>$row->field_details,
                        "instructions"=>$row->instructions,
                        "dict_status" =>$row->dict_status,
                );
                array_push($temp_det, $data_det);
            }
        }
        return $temp_det;
    }


    function update_dict_status()
    {
        $form_id = $this->input->post('form_id');
        $page_no = $this->input->post('page_no');
        $fid = $this->input->post('field_id');
        $status = $this->input->post('status');
        $result = $this->jobs->update_dict_status($form_id, $page_no, $fid, $status);
        // echo json_encode($result);
    }

}

?>
